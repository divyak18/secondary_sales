package com.squer.tools.codegen.generators.meta;

import com.squer.platform.services.entity.SquerReference;

/**
 * Created by ashutoshpavaskar on 02/12/16.
 */
public class AttributeMeta {

    private String name;
    private Class type;
    private String dbColumn;
    private String dbType;
    private String since;
    private String mapped;


    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getType() {
        return type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public String getDbColumn() {
        return dbColumn;
    }

    public void setDbColumn(String dbColumn) {
        this.dbColumn = dbColumn;
    }

    public String getDbType() {
        return dbType;
    }


    public String getMapped() {
        if(SquerReference.class.isAssignableFrom(type))
            return name + ".id";

        if(this.mapped.equalsIgnoreCase("")){
            return name;
        }
        return  mapped;
    }

    public void setMapped(String mapped) {
        this.mapped = mapped;
    }


    public String getTypeHandler(){
        if(SquerReference.class.isAssignableFrom(type))
            return "com.squer.platform.persistence.impl.ReferenceTypeHandler";
        else
            return "";
    }

    public void setDbType(String dbType) {
        switch (dbType.toUpperCase()){
            case "L_STRING":
                this.dbType = "VARCHAR(255)";
                break;
            case "M_STRING":
                this.dbType = "VARCHAR(100)";
                break;
            case "S_STRING":
                this.dbType = "VARCHAR(25)";
                break;
            case "DATE":
                this.dbType = "DATE";
                break;
            case "T_STAMP":
                this.dbType = "TIMESTAMP WITH TIME ZONE";
                break;
            case "LONG":
                this.dbType = "BIGINT";
                break;
            case "ID":
                this.dbType = "CHAR(37)";
                break;
            case "BOOLEAN":
                this.dbType = "BOOLEAN";
                break;
            default:{
                if(!dbType.equalsIgnoreCase(""))
                    this.dbType = dbType;
                else
                    this.dbType = "VARCHAR(255)";
            }

        }
    }
}
