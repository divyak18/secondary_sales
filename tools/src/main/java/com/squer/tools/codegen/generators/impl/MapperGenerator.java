package com.squer.tools.codegen.generators.impl;

import com.squer.tools.codegen.generators.CodeGenerator;
import com.squer.tools.codegen.generators.meta.EntityMeta;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 30/11/16.
 */
public class MapperGenerator implements CodeGenerator{
    private static String mapperHeader =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<!DOCTYPE mapper PUBLIC \"-//mybatis.org//DTD Mapper 3.0//EN\" \"http://mybatis.org/dtd/mybatis-3-mapper.dtd\">\n";

    @Override
    public void generate(List<EntityMeta> entities, String version) throws Exception {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class",ClasspathResourceLoader.class.getName());
        ve.init();
        Template t = ve.getTemplate( "templates/mapper.vm" );

        Map<String, String> mapper = new HashMap<>();
        Map<String, List<EntityMeta>> entityModuleMap = new HashMap<>();
        String installPWD = System.getProperty("install.path");
        System.out.println("================Generating Entity meta=======================");
        for(EntityMeta entityMeta : entities) {
            List<EntityMeta> metas = new ArrayList<>();
            if(entityModuleMap.containsKey(entityMeta.getModule())) {
                metas= entityModuleMap.get(entityMeta.getModule());
            }
            metas.add(entityMeta);
            entityModuleMap.put(entityMeta.getModule(), metas);
        }

        for(Map.Entry<String, List<EntityMeta>> entry : entityModuleMap.entrySet()){
            List<EntityMeta> metas = entry.getValue();
            String moduleMaps = "";
            for(EntityMeta meta : metas) {
                StringWriter writer = new StringWriter();
                VelocityContext context = new VelocityContext();
                System.out.println("Entity" + meta.getClazz().getName());
                context.put("entity", meta);
                t.merge(context, writer);
                moduleMaps = moduleMaps + writer.toString();
            }
            String outPath = getOutputPath(installPWD, entry.getKey());
            String out = mapperHeader  + "\n" +
                    "<mapper namespace=\"" + entry.getKey() + "\">\n" + moduleMaps + "\n</mapper>";
            Files.write(Paths.get(outPath), out.getBytes());


        }
    }

    private String getOutputPath(String installPWD, String module){
        File file = new File( installPWD + "/" + module + "/src/main/resources/mapper");
        if(file.exists())
            return  installPWD + "/" + module + "/src/main/resources/mapper/" + module + "-generated-mapper.xml";
        file.mkdir();
        return  installPWD + "/" + module + "/src/main/resources/mapper/" + module + "-generated-mapper.xml";
    }

}
