package com.squer.tools.codegen.generators;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.*;

/**
 * Created by ashutoshpavaskar on 03/12/16.
 */
public class DbRunner {

    public static void main(String[] args) throws Exception{
        Connection connection = null;
        try {
            String version = "1.0";
            String basePath = System.getProperty("install.path");
            String installPWD = basePath + "/database/" + version + "/";


            List<String> sqls = getSql(installPWD + "ddl.sql");

            connection = getConnection(basePath);
            for (String sql : sqls) {
                System.out.println("SQL:" + sql);
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }

            sqls = getSql(installPWD + "alter.sql");
            for (String sql : sqls) {
                System.out.println("SQL:" + sql);
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }

            sqls = getSql(installPWD + "adhoc-ddl.sql");
            for (String sql : sqls) {
                System.out.println("SQL:" + sql);
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }

            sqls = getSql(installPWD + "dml.sql");
            for (String sql : sqls) {
                System.out.println("SQL:" + sql);
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }

        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }finally {
            connection.close();
        }
    }

    private static List<String> getSql(String filePath) throws Exception{
        File file = new File(filePath);
        if (!file.exists())
            return  new ArrayList<>();
        String allContent = new String(Files.readAllBytes(Paths.get(filePath)));
        StringTokenizer tokenizer =  new StringTokenizer(allContent,";");
        List<String> sqls = new ArrayList<>();
        while(tokenizer.hasMoreTokens())
            sqls.add(tokenizer.nextToken());
        return sqls;
    }

    private static Connection getConnection(String installPWD) throws Exception{
        System.out.println("==================>");
        Properties resource = new Properties();
        File file = new File(installPWD + "/config/database.properties");
        resource.load(new FileInputStream(file));

        String URL = resource.getProperty("jdbc.db_url");
        Properties info = new Properties( );
        info.put( "user", resource.getProperty("jdbc.username") );
        info.put( "password", resource.getProperty("jdbc.password"));
        Class.forName(resource.getProperty("jdbc.driverClassName"));
        System.out.println("============>" + URL);
        Connection conn = DriverManager.getConnection(URL, info);
        return  conn;
    }

}
