package com.squer.tools.codegen.generators;

import com.squer.tools.codegen.generators.meta.EntityMeta;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 01/12/16.
 */
public interface CodeGenerator {

    public void generate(List<EntityMeta> entities, String version) throws Exception;
}
