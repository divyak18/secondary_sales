package com.squer.tools.codegen.generators.impl;

import com.squer.tools.codegen.generators.CodeGenerator;
import com.squer.tools.codegen.generators.meta.EntityMeta;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 30/11/16.
 */
public class ReferenceMetaGenerator implements CodeGenerator{
    @Override
    public void generate(List<EntityMeta> entities, String version) throws Exception {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class",ClasspathResourceLoader.class.getName());
        ve.init();
        Template t = ve.getTemplate( "templates/reference.vm" );

        System.out.println("================Generating Reference meta=======================");
        String installPWD = System.getProperty("install.path");

        Map<String, String> mapper = new HashMap<>();
        for(EntityMeta entityMeta : entities){
            System.out.println("Entity" + entityMeta.getClazz().getName());
            StringWriter writer = new StringWriter();
            VelocityContext context = new VelocityContext();
            context.put("entity", entityMeta);
            t.merge( context, writer );
            String out = "";
            if(mapper.containsKey(entityMeta.getModule())) {
                out = mapper.get(entityMeta.getModule());
            }
            out = out +  "\n" + writer.toString();
            mapper.put(entityMeta.getModule(),  out);
        }

        for(Map.Entry<String, String> entry : mapper.entrySet()){
            String outPath = getOutputPath(installPWD, entry.getKey());
            String out = "<references>" + entry.getValue() + "\n</references>";
            Files.write(Paths.get(outPath), out.getBytes());
        }

    }


    private String getOutputPath(String installPWD, String module){
        File file = new File( installPWD + "/" + module + "/src/main/resources/reference");
        if(file.exists())
            return  installPWD + "/" + module + "/src/main/resources/reference/" + module + "-reference-meta.xml";
        file.mkdir();
        return  installPWD + "/" + module + "/src/main/resources/reference/" + module + "-reference-meta.xml";
    }
}
