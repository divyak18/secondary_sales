package com.squer.tools.codegen.generators.impl;

import com.squer.tools.codegen.generators.CodeGenerator;
import com.squer.tools.codegen.generators.meta.AttributeMeta;
import com.squer.tools.codegen.generators.meta.EntityMeta;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 30/11/16.
 */
public class DDLGenerator implements CodeGenerator{
    @Override
    public void generate(List<EntityMeta> entities, String version) throws Exception {
        VelocityEngine ve = new VelocityEngine();
        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class",ClasspathResourceLoader.class.getName());
        ve.init();
        List<String> ddls = new ArrayList<>();
        List<String> alters = new ArrayList<>();
        List<String> pks  = new ArrayList<>();
        for(EntityMeta entityMeta : entities){
            if(entityMeta.getSince().equalsIgnoreCase(version)) {
                ddls.add(writeToDDL(ve, entityMeta));
            }else{
                for(AttributeMeta meta: entityMeta.getAttributes()) {
                    if(meta.getSince().equalsIgnoreCase(version))
                        alters.add(writeToAlter(ve, entityMeta.getTableName(), meta ));
                }
            }

            pks.add("ALTER TABLE " + entityMeta.getTableName() + " ADD PRIMARY KEY (ID);\n");
        }
        String installPWD = System.getProperty("install.path");
        System.out.println("Installing ddl path:" + installPWD);
        String outPath = getOutputPath(installPWD, version) + "/ddl.sql";
        String out=  "";
        for(String ddl : ddls){
            out = out + ddl;
        }
        Files.write(Paths.get(outPath), out.getBytes());

        outPath = getOutputPath(installPWD, version) + "/alter.sql";
        out=  "";
        for(String alter : alters){
            out = out + alter;
        }
        for(String pk : pks)
            out = out + pk;

        Files.write(Paths.get(outPath), out.getBytes());

    }

    private String writeToDDL(VelocityEngine ve, EntityMeta entity) throws Exception{
        Template t = ve.getTemplate( "templates/ddl.vm" );
        VelocityContext context = new VelocityContext();
        context.put("entity", entity);
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        return writer.toString();
    }

    private String writeToAlter(VelocityEngine ve, String tableName, AttributeMeta attributeMeta) throws Exception{
        Template t = ve.getTemplate( "templates/alter.vm" );
        VelocityContext context = new VelocityContext();
        Map<String, Object> map = new HashMap<>();
        map.put("tableName", tableName);
        map.put("attribute", attributeMeta);
        context.put("entity", map);
        StringWriter writer = new StringWriter();
        t.merge( context, writer );
        return writer.toString();
    }

    private String getOutputPath(String installPWD, String version){
        return  installPWD + "/database/" + version + "/";
    }
}
