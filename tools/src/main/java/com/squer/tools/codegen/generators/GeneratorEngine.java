package com.squer.tools.codegen.generators;


import com.squer.platform.SystemLovConstants;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.tools.codegen.generators.impl.DDLGenerator;
import com.squer.tools.codegen.generators.impl.MapperGenerator;
import com.squer.tools.codegen.generators.impl.ReferenceMetaGenerator;
import com.squer.tools.codegen.generators.meta.AttributeMeta;
import com.squer.tools.codegen.generators.meta.EntityMeta;
import org.reflections.ReflectionUtils;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by ashutoshpavaskar on 01/12/16.
 */
public class GeneratorEngine {



    public static void main(String[] args) throws Exception{
        String version = "1.0";
        Reflections reflections = new Reflections("com.squer");
        Set<Class<?>> subTypes = reflections.getTypesAnnotatedWith(Entity.class);
        List<EntityMeta> entities = new ArrayList<>();
        for(Class type : subTypes) {
            System.out.println("Entity:" + type.getName());
            Entity entity = (Entity) type.getAnnotation(Entity.class);
            EntityMeta meta = new EntityMeta();
            meta.setModule(entity.module());
            meta.setTableName(entity.table());
            meta.setClazz(type);
            meta.setSince(entity.since());
            meta.setPrefix(entity.prefix());
            meta.setModel(entity.model());

            Set<Field> fields = ReflectionUtils.getAllFields(type, ReflectionUtils.withAnnotation(Attribute.class));
            List<AttributeMeta> attributes = new ArrayList<>();
            for (Field field : fields) {
                AttributeMeta attribute = new AttributeMeta();
                Attribute annot = field.getDeclaredAnnotation(Attribute.class);
                attribute.setName(field.getName());
                attribute.setDbColumn(annot.column());
                attribute.setDbType(annot.dbType());
                attribute.setType(field.getType());
                attribute.setMapped(annot.mapped());
                attributes.add(attribute);
            }
            meta.setAttributes(attributes);
            entities.add(meta);
        }
        CodeGenerator generator = new DDLGenerator();
        generator.generate(entities, version);

        generator = new MapperGenerator();
        generator.generate(entities, version);

        generator = new ReferenceMetaGenerator();
        generator.generate(entities, version);

    }
}
