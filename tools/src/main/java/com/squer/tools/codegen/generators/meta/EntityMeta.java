package com.squer.tools.codegen.generators.meta;

import com.squer.platform.services.entity.NamedEntity;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 01/12/16.
 */
public class EntityMeta {

    private String since;
    private Class clazz;
    private String tableName;
    private String module;
    private String prefix;
    private String model;

    public String getPrefix() {
        return prefix.toLowerCase();
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    private List<AttributeMeta> attributes;


    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public List<AttributeMeta> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<AttributeMeta> attributes) {
        this.attributes = attributes;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getReferenceClassName(){
        String packageName = clazz.getPackage().getName();
        String clazzName = clazz.getSimpleName();
        return packageName + ".reference." + clazzName +  "Reference";
    }

    public String getReferenceClassType(){
        if(NamedEntity.class.isAssignableFrom(clazz))
            return "named";
        else
            return "simple";
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
