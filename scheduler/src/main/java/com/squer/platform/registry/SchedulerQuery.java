package com.squer.platform.registry;


import com.squer.platform.persistence.FinderQuery;

/**
 * Created by ashutoshpavaskar on 26/07/15.
 */
public enum SchedulerQuery implements FinderQuery {

    ScheduleQuery("schdr_select"),
    ScheduleStatusUpdate("schdr_status_update");

    private String id;
    private String sortField;
    private String sortOrder;

    private SchedulerQuery(String id){
        this.id = id;
    }

    private  SchedulerQuery(String id,String sortField,String sortOrder){
        this.id=id;
        this.sortField=sortField;
        this.sortOrder=sortOrder;
    }

    public String id(){
        return id;
    }

    public String sortField() {
        return sortField;
    }

    public String sortOrder() {
        return sortOrder;
    }
}
