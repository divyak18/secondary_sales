package com.squer.platform.registry;


import com.squer.platform.persistence.Delegates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */
public class SchedulerDelegates implements Delegates {
    private static Map<String, Delegates> delegatesMap = new HashMap<>();

    public static final Delegates Scheduler = new SchedulerDelegates("scheduler");

    private String id;
    private SchedulerDelegates(){

    }
    private SchedulerDelegates(String id){
        this.id = id;
        delegatesMap.put(id, this);
    }

    public String id(){
        return id;
    }

    public Delegates getDelegate(String id){
        return  delegatesMap.get(id);
    }
}
