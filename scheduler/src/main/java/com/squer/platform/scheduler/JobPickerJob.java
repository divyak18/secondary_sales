package com.squer.platform.scheduler;

import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.persistence.dao.QueryCondition;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.registry.SchedulerDelegates;
import com.squer.platform.registry.SchedulerQuery;
import com.squer.platform.scheduler.entity.ScheduleEntity;

import com.squer.platform.scheduler.entity.ScheduleJob;
import com.squer.platform.security.AuthenticationHandler;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.log.SquerLogger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */
public class JobPickerJob implements Job {
    public static final String NEW_STATUS = "NEW";
    public static final String ACTIVE_STATUS = "ACTIVE";
    public static final String INACTIVE_STATUS = "INACTIVE";
    public static final String TERMINATED_STATUS = "TERMINATED";

    public static final String INTERVAL_MINUTE = "MINUTES";
    public static final String INTERVAL_HOUR = "HOUR";
    public static final String INTERVAL_DAY = "DAY";
    public static final String INTERVAL_MONTH = "MONTH";

    SquerLogger logger = SquerLogger.getLogger(SchedulerLogger.JOBPICKER_JOB);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            System.out.println("Started the job picker");
            AuthenticationHandler.authenticateAsSchedulerAdmin();
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            Scheduler scheduler = ((StdSchedulerFactory)ServiceLocator.getInstance().getBean(SchedulerDelegates.Scheduler)).getScheduler();

            List<ScheduleEntity> activeSchedules = getActiveSchedules(repository);
            System.out.println("Number of Active Schedules:" + activeSchedules.size());
            if(activeSchedules!=null && !activeSchedules.isEmpty()){
                for(ScheduleEntity scheduleEntity: activeSchedules)
                    activateSchedule(repository, scheduler, scheduleEntity);
            }

        }catch (Exception e){
            throw new JobExecutionException(e);
        }
    }


    private List<ScheduleEntity> getActiveSchedules(Repository repository) throws Exception{
        CriteriaMap criteria = new CriteriaMap(SchedulerQuery.ScheduleQuery);
        List<QueryCondition> conditions = new ArrayList<>();
        conditions.add(new QueryCondition("status", NEW_STATUS));
        criteria.addCriteria("searchQuery", conditions);
        return repository.find(criteria);
    }

    private void activateSchedule(Repository repository, Scheduler scheduler, ScheduleEntity schedule) throws Exception{
        JobKey key = new JobKey(schedule.getReference().getId(), schedule.getGroupName());
        if(scheduler.getJobDetail(key)!=null){
            scheduler.deleteJob(key);
        }
        ScheduleJob sJob = repository.restore(schedule.getJob());
        Class<? extends Job> jobClass = (Class<Job>)Class.forName(sJob.getExecClass());
        JobDetail job = JobBuilder
                .newJob(jobClass)
                .withIdentity(key)
                .usingJobData("scheduleId", schedule.getReference().getId())
                .build();

        logger.debug("*********************************************************************");
        logger.debug("Activated logger for:" + schedule.getReference().getId() + "=" + schedule.getJob());
        logger.debug("*********************************************************************");

        String startTime = schedule.getStartTime();
        String hrs = startTime.substring(0,2);
        String mns = startTime.substring(3);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(schedule.getStartDate());
        calendar.set(Calendar.HOUR,Integer.parseInt(hrs));
        calendar.set(Calendar.MINUTE, Integer.parseInt(mns));
        Trigger simpleTrigger = buildTrigger(schedule, calendar, job);
        logger.debug("*********************************************************************");
        logger.debug("SCHEDULED JOB:" + schedule.getReference().getId() + " to " + simpleTrigger.getStartTime() + "=" + simpleTrigger.getNextFireTime());
        logger.debug("*********************************************************************");

        scheduler.scheduleJob(job, simpleTrigger);
        schedule.setStatus(ACTIVE_STATUS);
        repository.update(schedule);
    }


    private Trigger buildTrigger(ScheduleEntity schedule, Calendar startTime, JobDetail job){
        System.out.println("Scheduling job:" + schedule.getReference().getId() + "," + schedule.getFrequencyUnit() + ", " + schedule.getFrequency());
        switch (schedule.getFrequencyUnit()){
            case INTERVAL_MINUTE:
                return TriggerBuilder.newTrigger()
                        .withIdentity(schedule.getReference().getId(),"Scheduler")
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInMinutes(schedule.getFrequency())
                        .repeatForever()
                        .withMisfireHandlingInstructionFireNow())
                        .startAt(startTime.getTime())
                        .forJob(job)
                        .build();
            case INTERVAL_HOUR:
                return TriggerBuilder.newTrigger()
                        .withIdentity(schedule.getReference().getId(),"Scheduler")
                        .withSchedule(SimpleScheduleBuilder.simpleSchedule()
                        .withIntervalInHours(schedule.getFrequency())
                        .repeatForever()
                        .withMisfireHandlingInstructionFireNow())
                        .startAt(startTime.getTime())
                        .forJob(job)
                        .build();
            case INTERVAL_DAY:
                return TriggerBuilder.newTrigger()
                        .withIdentity(schedule.getReference().getId(), "Scheduler")
                        .withSchedule(CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInDays(schedule.getFrequency()).withMisfireHandlingInstructionFireAndProceed())
                        .startAt(startTime.getTime())
                        .forJob(job)
                        .build();
            case INTERVAL_MONTH:
                return TriggerBuilder.newTrigger()
                        .withIdentity(schedule.getReference().getId(), "Scheduler")
                        .withSchedule(CalendarIntervalScheduleBuilder.calendarIntervalSchedule().withIntervalInMonths(schedule.getFrequency()).withMisfireHandlingInstructionFireAndProceed())
                        .startAt(startTime.getTime())
                        .forJob(job)
                        .build();
            default:
                throw new RuntimeException("Unknown schedule interval" + schedule.getFrequencyUnit());
        }
    }
}
