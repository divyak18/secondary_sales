package com.squer.platform.scheduler.entity;

import com.squer.platform.scheduler.entity.reference.ScheduleJobReference;
import com.squer.platform.services.entity.impl.StandardEntity;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */
@Entity(prefix = "SCHJB", module = "scheduler", since = "1.0", table="SCH_JOB_DEFINITION")
public class ScheduleJob extends StandardEntity {

    @Attribute(column = "exec_class",  dbType = "L_STRING")
    private String execClass;

    public String getExecClass() {
        return execClass;
    }

    public void setExecClass(String execClass) {
        this.execClass = execClass;
    }


}
