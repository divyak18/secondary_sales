package com.squer.platform.scheduler.textui;

import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.registry.SchedulerDelegates;
import com.squer.platform.registry.SchedulerQuery;
import com.squer.platform.scheduler.JobPickerJob;
import com.squer.platform.scheduler.SchedulerLogger;
import com.squer.platform.security.AuthenticationHandler;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.log.SquerLogger;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */
public class SchedulerEngine {
    private static SquerLogger logger = SquerLogger.getLogger(SchedulerLogger.JOBPICKER_JOB);

    public static void main(String[] args) throws Exception{
        try {

            AuthenticationHandler.authenticateAsSchedulerAdmin();
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap initializeSchedules = new CriteriaMap(SchedulerQuery.ScheduleStatusUpdate);
            repository.fireAdhoc(initializeSchedules);

            logger.debug("*****************************");
            logger.debug("Scheduler initiated");
            logger.debug("*****************************");
            StdSchedulerFactory schedulerFactory = (StdSchedulerFactory)ServiceLocator.getInstance().getBean(SchedulerDelegates.Scheduler);
            Scheduler scheduler = schedulerFactory.getScheduler();
            scheduler.start();
            JobDetail job = scheduler.getJobDetail(new JobKey("JobPicker"));
            if(job!=null)
                scheduler.deleteJob(new JobKey("JobPicker"));
            logger.debug("Scheduler started");
            job = JobBuilder
                    .newJob(JobPickerJob.class)
                    .withIdentity("JobPicker")
                    .build();
            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity("JobPicker")
                    .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(120).repeatForever())
                    .build();
            scheduler.scheduleJob(job, trigger);
        }catch (Exception e){
            logger.error(e);
        }
    }



}
