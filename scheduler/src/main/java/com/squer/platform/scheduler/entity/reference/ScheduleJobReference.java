package com.squer.platform.scheduler.entity.reference;


import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */

public class ScheduleJobReference  extends SquerNamedReferenceImpl {

    public ScheduleJobReference(){

    }

    public ScheduleJobReference(String id) {
        super(id, null);
    }

    public ScheduleJobReference(String id, String name){
        super(id, name);
    }
}
