package com.squer.platform.scheduler.entity;

import com.squer.platform.scheduler.entity.reference.ScheduleEntityReference;
import com.squer.platform.scheduler.entity.reference.ScheduleJobReference;
import com.squer.platform.services.entity.impl.AuditableImpl;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;

import java.util.Date;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */
@Entity(prefix = "SCHDR", module = "scheduler", since = "1.0", table="SCH_SCHEDULE_DEFINITION")
public class ScheduleEntity extends AuditableImpl {

    @Attribute(column = "job_id",  dbType = "ID", mapped = "job.id")
    private ScheduleJobReference job;

    @Attribute(column = "group_name",  dbType = "M_STRING")
    private String groupName;

    @Attribute(column = "start_date",  dbType = "DATE")
    private Date startDate;

    @Attribute(column = "end_date",  dbType = "DATE")
    private Date endDate;

    @Attribute(column = "start_time",  dbType = "S_STRING")
    private String startTime;

    @Attribute(column = "schedule_frequency",  dbType = "LONG")
    private int frequency;

    @Attribute(column = "frequency_unit",  dbType = "S_STRING")
    private String frequencyUnit;

    @Attribute(column = "status",  dbType = "M_STRING")
    private String status;


    public ScheduleJobReference getJob() {
        return job;
    }

    public void setJob(ScheduleJobReference job) {
        this.job = job;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getFrequencyUnit() {
        return frequencyUnit;
    }

    public void setFrequencyUnit(String frequencyUnit) {
        this.frequencyUnit = frequencyUnit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
