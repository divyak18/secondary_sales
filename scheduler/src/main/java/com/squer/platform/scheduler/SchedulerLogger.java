package com.squer.platform.scheduler;

import com.squer.platform.services.log.LoggerName;

/**
 * Created by ashutoshpavaskar on 24/12/16.
 */
public enum SchedulerLogger implements LoggerName{

    JOBPICKER_JOB("jobpicker");

    private String name;
    private SchedulerLogger(String name){
        this.name = name;
    }

    @Override
    public String logger() {
        return name;
    }

}


