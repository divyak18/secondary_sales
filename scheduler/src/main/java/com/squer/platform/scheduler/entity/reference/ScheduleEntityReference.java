package com.squer.platform.scheduler.entity.reference;

import com.squer.platform.services.entity.impl.SquerReferenceImpl;

/**
 * Created by ashutoshpavaskar on 25/07/15.
 */
public class ScheduleEntityReference extends SquerReferenceImpl {

    public ScheduleEntityReference() {
        super(null);
    }

    public ScheduleEntityReference(String id) {
        super(id);
    }
}
