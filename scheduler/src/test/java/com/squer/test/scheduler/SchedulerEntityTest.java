package com.squer.test.scheduler;

import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.scheduler.entity.ScheduleEntity;
import com.squer.platform.scheduler.entity.ScheduleJob;
import com.squer.platform.scheduler.entity.reference.ScheduleEntityReference;
import com.squer.platform.scheduler.entity.reference.ScheduleJobReference;
import com.squer.platform.security.AuthenticationHandler;
import com.squer.platform.security.ServiceLocator;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Date;

/**
 * Created by ashutoshpavaskar on 25/12/16.
 */
public class SchedulerEntityTest {



    // @Test(groups={"scheduler"})
    public void testCreate(){
        try{
            AuthenticationHandler.authenticateAsGuest();
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);

            ScheduleJob job = new ScheduleJob();
            job.setName("Test Job");
            job.setExecClass("com.squer.scheduler.Job");
            ScheduleJobReference jobReference = repository.create(job);

            ScheduleEntity schedule= new ScheduleEntity();
            schedule.setFrequency(5);
            schedule.setFrequencyUnit("MINUTES");
            schedule.setStatus("ACTIVE");
            schedule.setStartDate(new Date());
            schedule.setStartTime("10:00");
            schedule.setJob(jobReference);
            ScheduleEntityReference reference = repository.create(schedule);
        }catch(Exception e){
            Assert.fail("Create failed", e);
        }
    }

}
