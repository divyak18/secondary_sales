package com.squer.sales.parser.stockist;

import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.mail.MailLoggerEnum;
import com.squer.sales.mail.MailSystemPropertiesEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Stockist5617 implements StatementParser {

    private boolean complete;
    private Double sales;
    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.ParserListener);
    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
    @Override
    public boolean isValidStatement(String[] lines) throws Exception {

        if(lines.length < 7)
            return false;
        String line = lines[0].trim();
        System.out.println("---->" + line);
        if(!line.startsWith("ROY AGENCY")) {
            logger.info("The file is not for ROY");
            return false;
        }
        line = lines[1].trim();
        if(!line.startsWith("284,DIAMOND HARBOUR ROAD")) {
            logger.info("The file address is invalid");
            return false;
        }
        for(String l : lines){
            line = l.trim();
            if (line.startsWith("Sales & Stock Statement")) {
                checkStatementPeriod(line);
            }
            System.out.println(line + "-------" + line.startsWith("TOTAL"));
            if (line.startsWith("TOTAL")) {
                /*
                line = line.substring("Sales :".length()).trim();
                line = line.substring(0, line.indexOf(' ')).trim();
                */
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                int i =0;
                String value = "0";
                while(tokenizer.hasMoreTokens()){
                    String token = tokenizer.nextToken();
                    if (i == 7) {
                        value =  token;
                    }
                    i++;
                }
                this.sales = Double.parseDouble(value);
            }
        }
        return true;
    }

    private void checkStatementPeriod(String line) throws ParseException {
        int month = Integer.parseInt(SystemProperties.getProperty(MailSystemPropertiesEnum.StatementMonth));
        String fromDate = line.substring("Sales & Stock Statement (From ".length(), line.lastIndexOf("Upto")).trim();
        String toDate = line.substring(line.lastIndexOf("Upto") + 5).trim();
        Date date = format.parse(fromDate);
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(date);
        int fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (fromMonth != month) {
            complete = false;
            return;
        }
        date = format.parse(toDate);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(date);
        int toMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (toMonth != month) {
            complete = false;
            return;
        }
        complete = true;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public Double getSales() {
        return sales;
    }
	@Override
    public String getStockistCode() {
        return "5617";
    }
   }
