package com.squer.sales.parser;

public class InvalidFileException extends Exception {

    public InvalidFileException(String message) {
        super(message);
    }
}
