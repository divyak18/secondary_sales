package com.squer.sales.parser;

import com.squer.sales.parser.stockist.StatementParser;

import java.io.File;
import java.util.List;

public interface SalesFileReader {

    public String readContent(File inputFile) throws Exception;

    public List<StatementParser> getParsers();

}