package com.squer.sales.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrimarySalesReader {
    private static final String[] teams = {
            "",
            "CARDIO",	"CHC BJ",	"CHC OTC-OTX",	"CHC WH", "CNS",	"DIABETES INSULIN",
            "DIABETES ORAL",	"HOSPITAL ANTI INF", "HOSPITAL THROMBO", "MASS THERAPY", "OTHERS", "TOTAL"
    };

    public Map<String, Map<String, Double>> readContent(File inputFile) throws Exception {
        //String fileToParse = "inputFile";
        Map<String, Map<String, Double>> salesMap = new HashMap<>();
        BufferedReader fileReader = null;
        final String DELIMITER = ",";
        String line = null;
        try {
            line = "";
            fileReader = new BufferedReader(new FileReader(inputFile));
            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(DELIMITER);
                String stockist = tokens[0];
                if ("Row Labels".equalsIgnoreCase(stockist)) continue;
                Map<String, Double> salesByTeam = new HashMap<>();
                for(int i =1; i< teams.length; i++) {
                    if ("".equalsIgnoreCase(tokens[i]))
                        salesByTeam.put(teams[i], 0.0);
                    else
                        salesByTeam.put(teams[i], Double.parseDouble(tokens[i]) * 1000);
                }
                salesMap.put(stockist, salesByTeam);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return salesMap;
    }
}