package com.squer.sales.parser.pdf;

import com.squer.sales.parser.SalesFileReader;
import com.squer.sales.parser.stockist.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PDFReader implements SalesFileReader {

    @Override
    public String readContent(File inputFile) throws Exception {

        PDDocument document = null;
        try {

            document = PDDocument.load(inputFile);
            String text = new PDFTextStripper().getText(document);
            System.out.println(text);
            return text;
/*
            String[] lines = text.split(System.getProperty("line.separator"));
            boolean dataStarted = false;
            String dataLine = "";
            for (String line : lines) {
                if (line.startsWith("From Date ")) {
                    Date[] dates = parseDate(line);
                    data.setFromDate(dates[0]);
                    data.setToDate(dates[1]);
                }
                if (line.startsWith("PRODUCT DESCRIPTION")) {
                    dataStarted = true;
                }
                if (line.startsWith("Division Total")) {
                    dataStarted = false;
                }
                if (dataStarted) {
                    dataLine = dataLine + line.replaceAll("\\s+", "");
                }
            }
*/

        }finally {
            if(document!=null)
                document.close();
        }
    }

    @Override
    public List<StatementParser> getParsers() {
        List<StatementParser> parsers = new ArrayList<>();


        parsers.add(new Stockist5198());












        /*
        parsers.add(new Stockist62575());
             parsers.add(new Stockist5261());
                parsers.add(new Stockist84203());

                parsers.(new Stockist5310());

              parsers.add(new Stockist83391());
        parsers.add(new Stockist5189());
         parsers.add(new Stockist69722());
        parsers.add(new Stockist63190());
        parsers.add(new Stockist62841());
                parsers.add(new Stockist62563());
         parsers.add(new Stockist6581());
        parsers.add(new Stockist6283());
          parsers.add(new Stockist6576());
        parsers.add(new Stockist6282());
        parsers.add(new Stockist6043());
        parsers.add(new Stockist5310());
        parsers.add(new Stockist5205());
        parsers.add(new Stockist5189());
        parsers.add(new Stockist5198());
        parsers.add(new Stockist5205());
        parsers.add(new Stockist6698());
        parsers.add(new Stockist14579());
        parsers.add(new Stockist63050());
        parsers.add(new Stockist63425());
        parsers.add(new Stockist69121());

        parsers.add(new Stockist84126());
        parsers.add(new Stockist84178());
        parsers.add(new Stockist84206());
        parsers.add(new Stockist86556());
        parsers.add(new Stockist111420());
        parsers.add(new Stockist113795());
        parsers.add(new Stockist121742());
        parsers.add(new Stockist14579());
        parsers.add(new Stockist5197());
        parsers.add(new Stockist5320());
        parsers.add(new Stockist5373());
        parsers.add(new Stockist5392());
        parsers.add(new Stockist5425());
        parsers.add(new Stockist5520());
        parsers.add(new Stockist5570());
        parsers.add(new Stockist5585());
        parsers.add(new Stockist5604());
        parsers.add(new Stockist5611());
        parsers.add(new Stockist5637());
        parsers.add(new Stockist5726());
        parsers.add(new Stockist5760());
        parsers.add(new Stockist5909());
        parsers.add(new Stockist6063());
        parsers.add(new Stockist6141());
        parsers.add(new Stockist6157());
        parsers.add(new Stockist6158());
        parsers.add(new Stockist6170());
        parsers.add(new Stockist6176());
        parsers.add(new Stockist6182());
        parsers.add(new Stockist6217());
        parsers.add(new Stockist6219());
        parsers.add(new Stockist6395());
        parsers.add(new Stockist6396());
        parsers.add(new Stockist6642());
        parsers.add(new Stockist6609());
        parsers.add(new Stockist6614());
        parsers.add(new Stockist6629());
        parsers.add(new Stockist6640());
        parsers.add(new Stockist6642());
        parsers.add(new Stockist6643());
        parsers.add(new Stockist6643_Sapragiri());
        parsers.add(new Stockist6655());
        parsers.add(new Stockist6684());
        parsers.add(new Stockist6685());
        parsers.add(new Stockist6695());
        parsers.add(new Stockist6696());
        parsers.add(new Stockist6708());
        parsers.add(new Stockist6710());
        parsers.add(new Stockist6720());
        parsers.add(new Stockist6723());
        parsers.add(new Stockist6755());
        parsers.add(new Stockist6777());
        parsers.add(new Stockist6808());
        parsers.add(new Stockist6976());
        parsers.add(new Stockist6978());
        parsers.add(new Stockist7236());
        parsers.add(new Stockist7270());
        parsers.add(new Stockist7282());
        parsers.add(new Stockist7303());
        parsers.add(new Stockist7304());
        parsers.add(new Stockist7434());
        parsers.add(new Stockist7473());
        parsers.add(new Stockist7479());
        parsers.add(new Stockist7501());
        parsers.add(new Stockist7550());
        parsers.add(new Stockist7603());
        parsers.add(new Stockist7653());
        parsers.add(new Stockist7705());
        parsers.add(new Stockist7727());
        parsers.add(new Stockist7737());
        parsers.add(new Stockist7884());
        parsers.add(new Stockist7885());
        parsers.add(new Stockist7892());
        parsers.add(new Stockist7926());
        parsers.add(new Stockist7958());
        parsers.add(new Stockist8057());
        parsers.add(new Stockist10415());
        parsers.add(new Stockist10916());
        parsers.add(new Stockist11090());
        parsers.add(new Stockist11370());
        parsers.add(new Stockist13938());
        parsers.add(new Stockist14623());
        parsers.add(new Stockist14665());
        parsers.add(new Stockist14717());
        parsers.add(new Stockist14768());
        parsers.add(new Stockist16517());
        parsers.add(new Stockist48171());
        parsers.add(new Stockist48171_saipurna());
        parsers.add(new Stockist48254());
        parsers.add(new Stockist48688());
        parsers.add(new Stockist50016());
        parsers.add(new Stockist50808());
        parsers.add(new Stockist50922());
        parsers.add(new Stockist50974());
        parsers.add(new Stockist51044());
        parsers.add(new Stockist61629());
        parsers.add(new Stockist62299());
        parsers.add(new Stockist62443());
        parsers.add(new Stockist62736());
        parsers.add(new Stockist62813());
        parsers.add(new Stockist62836());
        parsers.add(new Stockist62840());
        parsers.add(new Stockist62843());
        parsers.add(new Stockist62927());
        parsers.add(new Stockist62940());
        parsers.add(new Stockist63024());
        parsers.add(new Stockist63043());
        parsers.add(new Stockist63083());
        parsers.add(new Stockist63093());
        parsers.add(new Stockist63230());
        parsers.add(new Stockist63233());
        parsers.add(new Stockist63410());
        parsers.add(new Stockist63665());
        parsers.add(new Stockist63666());
        parsers.add(new Stockist63853());
        parsers.add(new Stockist63976());
        parsers.add(new Stockist63977());
        parsers.add(new Stockist64421());
        parsers.add(new Stockist64502());
        parsers.add(new Stockist64993());
        parsers.add(new Stockist65199());
        parsers.add(new Stockist65295());
        parsers.add(new Stockist65969());
        parsers.add(new Stockist68885());
        parsers.add(new Stockist69867());
        parsers.add(new Stockist69875());
        parsers.add(new Stockist69886());
        parsers.add(new Stockist74874());
        parsers.add(new Stockist75096());
        parsers.add(new Stockist77468_sri_baba());
        parsers.add(new Stockist78296());
        parsers.add(new Stockist82729());
        parsers.add(new Stockist83059());
        parsers.add(new Stockist83355());
        parsers.add(new Stockist83355_1());
        parsers.add(new Stockist83420());
        parsers.add(new Stockist83429());
        parsers.add(new Stockist83457());
        parsers.add(new Stockist83549());
        parsers.add(new Stockist83724());
        parsers.add(new Stockist83964());
        parsers.add(new Stockist84146());
        parsers.add(new Stockist84153());
        parsers.add(new Stockist84240());
        parsers.add(new Stockist85663());
        parsers.add(new Stockist86432());
        parsers.add(new Stockist86682());
        parsers.add(new Stockist86776());
        parsers.add(new Stockist87421());
        parsers.add(new Stockist87551());
        parsers.add(new Stockist87564());
        parsers.add(new Stockist87581_sri());
        parsers.add(new Stockist95957());
        parsers.add(new Stockist99560());
        parsers.add(new Stockist102939());
        parsers.add(new Stockist103007());
        parsers.add(new Stockist103008());
        parsers.add(new Stockist103009());
        parsers.add(new Stockist103117());
        parsers.add(new Stockist110775());
        parsers.add(new Stockist110776());
        parsers.add(new Stockist112252());
        parsers.add(new Stockist113537());
        parsers.add(new Stockist115717());
        parsers.add(new Stockist118152());
        parsers.add(new Stockist121683());
        parsers.add(new Stockist124150());
        parsers.add(new Stockist124158());
        parsers.add(new Stockist124158_venkatpadmvati());
        parsers.add(new Stockist124225());
        parsers.add(new Stockist124265());
        parsers.add(new Stockist125617());
        parsers.add(new Stockist126105());
        parsers.add(new Stockist_camscan());
        */
        return parsers;
    }
}