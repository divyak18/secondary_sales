package com.squer.sales.parser.stockist;

import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.mail.MailLoggerEnum;
import com.squer.sales.mail.MailSystemPropertiesEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Stockist123440 implements StatementParser {
    private boolean complete;
    private Double sales;
    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.ParserListener);
    private static final SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yy");

    @Override
    public boolean isValidStatement(String[] lines) throws Exception {
        if(lines.length < 5)
            return false;
        if(!lines[0].trim().startsWith("DELHI DISTRIBUTORS")) {
            logger.info("The file is not for DELHI");
            return false;
        }
        if(!lines[1].trim().startsWith("(PROP. DELHI MEDIART PVT. LTD.) B-23, II & III FLOOR GT KARNAL ROAD IND AREA DELHI")) {
            logger.info("The file address is invalid");
            return false;
        }
        for(String line : lines){
            line = line.trim();
            if (line.startsWith("FROM:")) {
                checkStatementPeriod(line);
            }
            if (line.startsWith("Total")) {
                String numbers = line.substring("Total".length());
                int lastIndex = line.lastIndexOf(' ');
                //StringTokenizer tokenizer = new StringTokenizer(numbers, " ");
                //String value = tokenizer.nextToken().trim();
                String value = line.substring(lastIndex);
                System.out.println("Value:" + value);
                this.sales = Double.parseDouble(value);
            }
        }
        return true;
    }

    private void checkStatementPeriod(String line) throws ParseException {
        System.out.println(line);
        int month = Integer.parseInt(SystemProperties.getProperty(MailSystemPropertiesEnum.StatementMonth));
        String fromDate = line.substring(line.indexOf(':') + 1, line.indexOf("TO:")).trim();
        String toDate = line.substring(line.lastIndexOf(':') + 1).trim();
        System.out.println(fromDate);
        System.out.println(toDate);
        Date date = format.parse(fromDate);
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(date);
        int fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (fromMonth != month) {
            complete = false;
            return;
        }
        date = format.parse(toDate);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(date);
        int toMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (toMonth != month) {
            complete = false;
            return;
        }
        complete = true;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public Double getSales() {
        return sales;
    }
	@Override
    public String getStockistCode() {
        return "123440";
    }
   }
