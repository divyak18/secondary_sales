package com.squer.sales.parser.stockist;

import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.mail.MailLoggerEnum;
import com.squer.sales.mail.MailSystemPropertiesEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Stockist6224 implements StatementParser {
    private boolean complete;
    private Double sales;
    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.ParserListener);
    private static final SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    @Override
    public boolean isValidStatement(String[] lines) throws Exception {

        if(lines.length < 6)
            return false;
        String line = lines[1].trim();
        System.out.println(line);
        if(!line.startsWith("INTERNATIONAL AGENCIES")) {
            logger.info("The file is not for INTERNATIONAL");
            return false;
        }
        line = lines[3].trim();
        if(!line.startsWith("IX/578-1 & 2, ERINJERI ANGADI, BROTHERS LANE, Phone - 0487 2442241,2421323")) {
            logger.info("The file address is invalid");
            return false;
        }
        for(String l : lines){
            l = l.trim();
            if (l.startsWith("Manufacturerwise Stock and Sales Report For Date")) {
                checkStatementPeriod(l);
            }
            l = l.trim();
            if (l.startsWith("Grant Total")) {
                //String numbers = l.substring("Grant Total".length());
                //StringTokenizer tokenizer = new StringTokenizer(numbers, " ");
                //String value = tokenizer.nextToken().trim();
                String value = l.substring(l.lastIndexOf(' ') + 1);
                System.out.println("Value=----" + value);
                this.sales = Double.parseDouble(value);
            }
        }
        return true;
    }

    private void checkStatementPeriod(String line) throws ParseException {
        System.out.println(line);
        int month = Integer.parseInt(SystemProperties.getProperty(MailSystemPropertiesEnum.StatementMonth));
        String fromDate = line.substring(line.indexOf("From") + 5, line.lastIndexOf("to")).trim();
        String toDate = line.substring(line.lastIndexOf(' ') + 1).trim();
        System.out.println(fromDate + "------------------------" + toDate);
        Date date = format.parse(fromDate);
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(date);
        int fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (fromMonth != month) {
            complete = false;
            return;
        }
        date = format.parse(toDate);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(date);
        int toMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (toMonth != month) {
            complete = false;
            return;
        }
        complete = true;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public Double getSales() {
        return sales;
    }
	@Override
    public String getStockistCode() {
        return "6224";
    }
   }
