package com.squer.sales.parser.excel;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.InputStream;
import java.util.List;

import com.squer.sales.parser.SalesFileReader;
import com.squer.sales.parser.stockist.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class ExcelReader implements SalesFileReader {
    @Override
    public String readContent(File inputFile) throws Exception {
        InputStream inputStream = new FileInputStream(
                inputFile);
        Workbook wb = WorkbookFactory.create(inputStream);
        String sheetValue = "";
        Sheet sheet = wb.getSheetAt(0);
        int maxRows = 10000;
        int maxCells = 1000;
        for(int i =0; i < maxRows;i++) {
            Row row = sheet.getRow(i);
            if (row != null) {
                for(int j =0;j < maxCells; j++) {
                    Cell cell = row.getCell(j);
                    if (cell!=null) {
                        String cellValue = getCellValueAsString(cell);
                        sheetValue = sheetValue.concat(" ").concat(cellValue) ;
                    }
                }
                sheetValue = sheetValue.concat(System.getProperty("line.separator"));
            }
        }
        return sheetValue;
    }

    @Override
    public List<StatementParser> getParsers() {
        List<StatementParser> parsers = new ArrayList<>();



        parsers.add(new Stockist6388());












        /*
        parsers.add(new Stockist126131());
        parsers.add(new Stockist125571());
        parsers.add(new Stockist83549());
parsers.add(new Stockist83463());
        parsers.add(new Stockist79546());
        parsers.add(new Stockist69563());
        parsers.add(new Stockist64143());
            parsers.add(new Stockist62308());
        parsers.add(new Stockist14697());
        parsers.add(new Stockist14694());

        parsers.add(new Stockist7699());
         parsers.add(new Stockist7480());
        parsers.add(new Stockist7470());
        parsers.add(new Stockist6381());
        parsers.add(new Stockist5905());
         parsers.add(new Stockist5617());
         parsers.add(new Stockist5276());
        parsers.add(new Stockist5385());
        parsers.add(new Stockist5565());
         parsers.add(new Stockist5276());

        parsers.add(new Stockist123440());
        parsers.add(new Stockist6224());
        parsers.add(new Stockist6220());
        parsers.add (new Stockist5364());

        parsers.add (new Stockist5883());
        parsers.add(new Stockist5907());
        parsers.add(new Stockist6179());
        parsers.add(new Stockist6191());
        parsers.add(new Stockist6392());
        parsers.add(new Stockist6393());
        parsers.add(new Stockist6719());
        parsers.add(new Stockist7040());
        parsers.add(new Stockist7257());
        parsers.add(new Stockist14686());
        parsers.add(new Stockist15935());
        parsers.add(new Stockist51795());
        parsers.add(new Stockist53678());
        parsers.add(new Stockist11796());
        parsers.add(new Stockist62707());
        parsers.add(new Stockist62739());
        parsers.add(new Stockist63034());
        parsers.add(new Stockist63245());
        parsers.add(new Stockist64782());
        parsers.add(new Stockist64992());
        parsers.add(new Stockist67018());
        parsers.add(new Stockist76854());
        parsers.add(new Stockist77514());
        parsers.add(new Stockist78720());
        parsers.add(new Stockist81051());
        parsers.add(new Stockist83075());
        parsers.add(new Stockist83228());
        parsers.add(new Stockist83365());
        parsers.add(new Stockist83431());
        parsers.add(new Stockist83571());
        parsers.add(new Stockist84147());
        parsers.add(new Stockist84219());
        parsers.add(new Stockist84273());
        parsers.add(new Stockist85902());
        parsers.add(new Stockist95930());
        parsers.add(new Stockist122382());
        parsers.add(new Stockist123441());
        parsers.add(new Stockist124245());
        parsers.add(new Stockist5203());
        parsers.add(new Stockist5385());
        parsers.add(new Stockist5475());
        parsers.add(new Stockist5812());
        parsers.add(new Stockist5935());
        parsers.add(new Stockist6133());
        parsers.add(new Stockist6233());
        parsers.add(new Stockist6238());
        parsers.add(new Stockist6240());
        parsers.add(new Stockist6287());
        parsers.add(new Stockist6327());
        parsers.add(new Stockist6381());
        parsers.add(new Stockist6771());
        parsers.add(new Stockist7216());
        parsers.add(new Stockist7232());
        parsers.add(new Stockist7237());
        parsers.add(new Stockist7263());
        parsers.add(new Stockist7277());
        parsers.add(new Stockist7298());
        parsers.add(new Stockist7682());
        parsers.add(new Stockist7715());
        parsers.add(new Stockist7761());
        parsers.add(new Stockist7781());
        parsers.add(new Stockist11371());
        parsers.add(new Stockist11380());
        parsers.add(new Stockist13947());
        parsers.add(new Stockist14627());
        parsers.add(new Stockist14655());
        parsers.add(new Stockist14691());
        parsers.add(new Stockist14881());
        parsers.add(new Stockist16049());
        parsers.add(new Stockist16235());
        parsers.add(new Stockist53813());
        parsers.add(new Stockist53855());
        parsers.add(new Stockist62257());
        parsers.add(new Stockist62490());
        parsers.add(new Stockist62509());
        parsers.add(new Stockist62587());
        parsers.add(new Stockist62771());
        parsers.add(new Stockist62791());
        parsers.add(new Stockist63027());
        parsers.add(new Stockist63030());
        parsers.add(new Stockist63032());
        parsers.add(new Stockist63048());
        parsers.add(new Stockist63167());
        parsers.add(new Stockist63223());
        parsers.add(new Stockist63232());
        parsers.add(new Stockist63289());
        parsers.add(new Stockist63368());
        parsers.add(new Stockist63427());
        parsers.add(new Stockist63676());
        parsers.add(new Stockist63749());
        parsers.add(new Stockist64457());
        parsers.add(new Stockist66425());
        parsers.add(new Stockist68009());
        parsers.add(new Stockist68222());
        parsers.add(new Stockist68223());
        parsers.add(new Stockist68710());
        parsers.add(new Stockist68924());
        parsers.add(new Stockist69874());
        parsers.add(new Stockist69885());
        parsers.add(new Stockist70629());
        parsers.add(new Stockist70836());
        parsers.add(new Stockist71098());
        parsers.add(new Stockist77577());
        parsers.add(new Stockist78383());
        parsers.add(new Stockist78721());
        parsers.add(new Stockist79288());
        parsers.add(new Stockist79429());
        parsers.add(new Stockist79642());
        parsers.add(new Stockist97639());
        parsers.add(new Stockist99721());
        parsers.add(new Stockist102948());
        parsers.add(new Stockist103015());
        parsers.add(new Stockist110777());
        parsers.add(new Stockist111687());
        parsers.add(new Stockist113608());
        parsers.add(new Stockist117510());
        parsers.add(new Stockist118590());
        parsers.add(new Stockist119130());
        parsers.add(new Stockist123464());
        parsers.add(new Stockist124214());
        parsers.add(new Stockist124284());
        parsers.add(new Stockist124285());
        parsers.add(new Stockist124332());
        parsers.add(new Stockist124337());
        parsers.add(new Stockist125672());
        parsers.add(new Stockist126407());
        parsers.add(new Stockist126408());
        parsers.add(new Stockist81064());
        parsers.add(new Stockist83057());
        parsers.add(new Stockist83345());
        parsers.add(new Stockist83357());
        parsers.add(new Stockist83414());
        parsers.add(new Stockist83421());

        parsers.add(new Stockist84014());
        parsers.add(new Stockist84123());
        parsers.add(new Stockist84168());
        parsers.add(new Stockist84174());
        parsers.add(new Stockist84191());
        parsers.add(new Stockist84200());
        parsers.add(new Stockist84225());
        parsers.add(new Stockist84244());
        parsers.add(new Stockist84266());
        parsers.add(new Stockist84351());
        parsers.add(new Stockist84426());
        parsers.add(new Stockist84926());
        parsers.add(new Stockist85073());
        parsers.add(new Stockist85330());
        parsers.add(new Stockist85340());
        parsers.add(new Stockist86090());
        parsers.add(new Stockist86832());
        parsers.add(new Stockist86838());
        parsers.add(new Stockist87124());
        parsers.add(new Stockist87550());
        parsers.add(new Stockist87878());
        parsers.add(new Stockist87977());
        */
        return parsers;

    }

    public static String getCellValueAsString(Cell cell) {
        String strCellValue = "";
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                strCellValue = cell.toString();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "dd/MM/YYYY");
                    strCellValue = dateFormat.format(cell.getDateCellValue());
                } else {
                    Double value = cell.getNumericCellValue();
                    //Long longValue = value.longValue();
                    strCellValue = String.valueOf(value);
                }
                break;
            case Cell.CELL_TYPE_FORMULA:
                strCellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                strCellValue = new String(new Boolean(
                        cell.getBooleanCellValue()).toString());
                break;
            case Cell.CELL_TYPE_BLANK:
                strCellValue = "";
                break;
        }
        return strCellValue;
    }
}













