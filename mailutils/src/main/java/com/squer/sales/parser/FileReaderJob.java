package com.squer.sales.parser;

import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.mail.MailSystemPropertiesEnum;
import com.squer.sales.parser.excel.CSVReader;
import com.squer.sales.parser.excel.ExcelReader;
import com.squer.sales.parser.pdf.PDFReader;
import java.lang.String;


import com.squer.sales.parser.stockist.*;
import com.squer.sales.parser.text.TextReader;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

public class FileReaderJob implements Job {

    private static String attachmentFolder = SystemProperties.getProperty(MailSystemPropertiesEnum.AttachmentSaveFolder);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            File baseFolder = new File(attachmentFolder + File.separator + "inputs");
            System.out.println("Base folder" + baseFolder.getAbsolutePath() + "---" + baseFolder.list());
            String mode = SystemProperties.getProperty(MailSystemPropertiesEnum.RunMode);
            String wipFolderPath = attachmentFolder + File.separator + "processed" + File.separator + "wip";
            boolean hasFiles = baseFolder.list().length > 0;

            File[] inputFiles = baseFolder.listFiles();


            PrimarySalesReader salesReader = new PrimarySalesReader();
            Map<String, Map<String, Double>> primarySales = salesReader.readContent(new File(attachmentFolder + "/SA_Customerwise_Sale_Apr18.csv"));
            if (hasFiles) {
                for (File inputFile : inputFiles) {
                    // File[] inputFiles = baseFolder.listFiles();
                    // hasFiles = inputFiles.length > 0;
                    // if (hasFiles) {
                    //File inputFile = inputFiles[0];
                    File wipFile = new File(wipFolderPath + File.separator + inputFile.getName());
                    if ("prod".equalsIgnoreCase(mode))
                        moveFileToWipFolder(wipFile, inputFile);
                    else
                        wipFile = inputFile;
                    String extn = getExtension(wipFile).toLowerCase();
                    SalesFileReader reader = null;
                    try {
                        switch (extn) {
                            case "pdf":
                                reader = new PDFReader();
                                break;
                            case "xls":
                                reader = new ExcelReader();
                                break;
                            case "csv":
                                reader = new CSVReader();
                                break;
                            case "txt":
                                reader = new TextReader();
                                break;
                            default:
                                throw new InvalidFileException("Unsupported file:" + extn);
                        }
                        String content = reader.readContent(wipFile);
                        String[] lines = content.split(System.getProperty("line.separator"));
                        List<StatementParser> parsers = reader.getParsers();

                        System.out.println("File being processed:" + inputFile.getName());
                        boolean foundParser = false;
                        for (StatementParser parser : parsers) {
                            if (parser.isValidStatement(lines)) {
                                System.out.println("Parser found:" + parser.getStockistCode() + "--" + parser.isComplete());
                                foundParser = true;
                                if (parser.isComplete()) {
                                    Double sales = primarySales.get(parser.getStockistCode()).get("TOTAL");
                                    if (parser.getSales() >= sales * .7) {
                                        System.out.println("Success" + parser.getSales() + "--" + sales);
                                        File successFile = new File(attachmentFolder + File.separator + "processed" + File.separator + "success" + File.separator + parser.getStockistCode() + "." + extn);
                                        moveFileToWipFolder(successFile, wipFile);
                                    } else {
                                        //TO DO move to error
                                        System.out.println("rejected" + parser.getSales() + "--" + sales);
                                        File successFile = new File(attachmentFolder + File.separator + "processed" + File.separator + "rejected" + File.separator + parser.getStockistCode() + "." + extn);
                                        moveFileToWipFolder(successFile, wipFile);
                                    }

                                }
                                break;
                            }
                        }
                        if (!foundParser) {
                            File successFile = new File(attachmentFolder + File.separator + "processed" + File.separator + "unparsed" + File.separator + wipFile.getName());
                            moveFileToWipFolder(successFile, wipFile);
                        }

                    } catch (InvalidFileException ie) {
                        //TODO
                        File successFile = new File(attachmentFolder + File.separator + "processed" + File.separator + "unparsed" + File.separator + wipFile.getName());
                        moveFileToWipFolder(successFile, wipFile);

                        ie.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void moveFileToWipFolder(File wipPath, File inputFile) throws IOException {
        Files.move(inputFile.toPath(), wipPath.toPath());
    }

    private String getExtension(File file) {
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
    }

}
