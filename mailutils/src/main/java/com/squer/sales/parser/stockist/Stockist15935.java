package com.squer.sales.parser.stockist;

import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.mail.MailLoggerEnum;
import com.squer.sales.mail.MailSystemPropertiesEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Stockist15935 implements StatementParser {
    private boolean complete;
    private Double sales;
    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.ParserListener);
    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    @Override
    public boolean isValidStatement(String[] lines) throws Exception {

        if(lines.length < 7)
            return false;
        String line = lines[1].trim();
        if(!line.startsWith("JEEKAY ENTERPRISES")) {
            logger.info("The file is not for S.P. MEDICAL");
            return false;
        }
        line = lines[3].trim();
        if(!line.startsWith("41/1309,C1,AMMNI BUILDING, C.P.UMMER CROSS ROAD, ERNAKULAM-35, ERNAKULAM, KOCHIN-682035, Phone - 4017476,4017477,4017479")) {
            logger.info("The file address is invalid");
            return false;
        }
        for(String l : lines){
            line = l.trim();
            if (line.startsWith("Stock and Sale Report")) {
                checkStatementPeriod(line);
            }
            System.out.println(line + "-------" + line.startsWith("Sales:"));
            if (line.startsWith("Sales :")) {
                line = line.substring("Sales :".length()).trim();
                line = line.substring(0, line.indexOf(' ')).trim();
                this.sales = Double.parseDouble(line);
            }
        }
        return true;
    }

    private void checkStatementPeriod(String line) throws ParseException {
        int month = Integer.parseInt(SystemProperties.getProperty(MailSystemPropertiesEnum.StatementMonth));
        String fromDate = line.substring(line.indexOf("date") + 5, line.lastIndexOf("to")).trim();
        String toDate = line.substring(line.lastIndexOf("to") + 3).trim();
        Date date = format.parse(fromDate);
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(date);
        int fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (fromMonth != month) {
            complete = false;
            return;
        }
        date = format.parse(toDate);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(date);
        int toMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (toMonth != month) {
            complete = false;
            return;
        }
        complete = true;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public Double getSales() {
        return sales;
    }
	@Override
    public String getStockistCode() {
        return "15935";
    }
   }
