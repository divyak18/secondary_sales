package com.squer.sales.parser.excel;
import com.squer.sales.parser.SalesFileReader;
import com.squer.sales.parser.stockist.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CSVReader implements SalesFileReader {

    @Override
    public String readContent(File inputFile) throws Exception {
        //String fileToParse = "inputFile";
        BufferedReader fileReader = null;
        final String DELIMITER = ",";
        String line = null;
        try {
            line = "";
            fileReader = new BufferedReader(new FileReader(inputFile));
            while ((line = fileReader.readLine()) != null) {
                String[] tokens = line.split(DELIMITER);
                for (String token : tokens) {


                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return line;
    }
    @Override
    public List<StatementParser> getParsers() {
        List<StatementParser> parsers = new ArrayList<>();
        parsers.add (new Stockist6146());
        parsers.add(new Stockist7481());
        parsers.add(new Stockist57216());
        parsers.add(new Stockist63105());
        parsers.add(new Stockist95949());
        parsers.add(new Stockist84709());
        parsers.add(new Stockist6569());
        parsers.add(new Stockist62713());
        return parsers;
    }
}

