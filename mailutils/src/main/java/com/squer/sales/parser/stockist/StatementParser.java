package com.squer.sales.parser.stockist;

public interface StatementParser {

    public boolean isValidStatement(String[] lines) throws Exception;

    public boolean isComplete();

    public Double getSales();

    public String getStockistCode();
}
