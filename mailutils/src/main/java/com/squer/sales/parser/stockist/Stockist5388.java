package com.squer.sales.parser.stockist;

import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.mail.MailLoggerEnum;
import com.squer.sales.mail.MailSystemPropertiesEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Stockist5388 implements StatementParser {
    private boolean complete;
    private Double sales;
    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.ParserListener);
    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    @Override
    public boolean isValidStatement(String[] lines) throws Exception {

        if(lines.length < 10)
            return false;
        if(!lines[0].contains("BALLABH AGENCIES, HOSHANGABAD")) {
            logger.info("The file is not for BALLABH");
            return false;
        }
        if(!lines[6].startsWith("HOSHANGABAD")) {
            logger.info("The file address is invalid");
            return false;
        }
        for(String line : lines){
            line = line.trim();
            if (line.startsWith("Stock statement ")) {
                checkStatementPeriod(line);
            }
            if (line.startsWith("Total in Value ")) {
                /*String numbers = line.substring("Division Total".length()); */

                StringTokenizer tokenizer = new StringTokenizer(line.substring("Total in Value :".length()), " ");
                // String value = tokenizer.nextToken().trim();
                int i =0;
                String value = "0";
                while (tokenizer.hasMoreTokens()) {
                    String token = tokenizer.nextToken();
                    System.out.println(token + "--" + i);
                    if (i == 3) {
                        value =  token;
                    }
                    i++;
                }
                this.sales = Double.parseDouble(value);
            }
        }
        return true;
    }

    private void checkStatementPeriod(String line) throws ParseException {
        int month = Integer.parseInt(SystemProperties.getProperty(MailSystemPropertiesEnum.StatementMonth));
        String fromDate = line.substring("Stock statement from".length() + 1, line.lastIndexOf("to")).trim();
        String toDate = line.substring(line.lastIndexOf("to") + 3, line.lastIndexOf(" Date")).trim();
        Date date = format.parse(fromDate);
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(date);
        int fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (fromMonth != month) {
            complete = false;
            return;
        }
        date = format.parse(toDate);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(date);
        int toMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (toMonth != month) {
            complete = false;
            return;
        }
        complete = true;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public Double getSales() {
        return sales;
    }
    @Override
    public String getStockistCode() {
        return "5388";
    }
}