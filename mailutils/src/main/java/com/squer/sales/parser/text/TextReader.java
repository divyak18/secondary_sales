package com.squer.sales.parser.text;

import com.squer.sales.parser.SalesFileReader;
import com.squer.sales.parser.stockist.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.*;

public class TextReader implements SalesFileReader {
    @Override
    public String readContent(File inputFile) throws Exception {
        String line = null;
        String content = "";
        try {
            FileReader fileReader = new FileReader(inputFile);
            BufferedReader bufferedReader =
                    new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
                content = content + line + "\n";
            }
            bufferedReader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    @Override
    public List<StatementParser> getParsers() {
        List<StatementParser> parsers = new ArrayList<>();
        parsers.add(new Stockist5456());

        /*
        parsers.add(new Stockist5388());
        parsers.add(new Stockist5377());
        parsers.add(new Stockist5388());
        parsers.add(new Stockist5391());
        parsers.add(new Stockist5411());
        parsers.add(new Stockist5393());
        parsers.add(new Stockist5438());
        parsers.add(new Stockist5488());
        parsers.add(new Stockist5498());
        parsers.add(new Stockist5512());
        parsers.add(new Stockist5590());
        parsers.add(new Stockist5636());
        parsers.add(new Stockist5680());
        parsers.add(new Stockist5995());
        parsers.add(new Stockist6042());
        parsers.add(new Stockist6412());
        parsers.add(new Stockist6428());
        parsers.add(new Stockist6567());
        parsers.add(new Stockist6570());
        parsers.add(new Stockist6573());
        parsers.add(new Stockist6577());
        parsers.add(new Stockist6643());
        parsers.add(new Stockist6687());
        parsers.add(new Stockist6689());
        parsers.add(new Stockist6690());
        parsers.add(new Stockist6725());
        parsers.add(new Stockist6728());
        parsers.add(new Stockist6760());
        parsers.add(new Stockist6770());
        parsers.add(new Stockist7015());
        parsers.add(new Stockist7027());
        parsers.add(new Stockist7031());
        parsers.add(new Stockist7032());
        parsers.add(new Stockist7035());
        parsers.add(new Stockist7437());
        parsers.add(new Stockist7463());
        parsers.add(new Stockist7551());
        parsers.add(new Stockist14592());
        parsers.add(new Stockist14593());
        parsers.add(new Stockist14600());
        parsers.add(new Stockist48046());
        parsers.add(new Stockist48435());
        parsers.add(new Stockist49012());
        parsers.add(new Stockist52081());
        parsers.add(new Stockist62724());
        parsers.add(new Stockist62859());
        parsers.add(new Stockist62915());
        parsers.add(new Stockist63415());
        parsers.add(new Stockist63439());
        parsers.add(new Stockist63593());
        parsers.add(new Stockist65502());
        parsers.add(new Stockist68367());
        parsers.add(new Stockist68398());
        parsers.add(new Stockist68617());
        parsers.add(new Stockist68853());
        parsers.add(new Stockist69886());
        parsers.add(new Stockist70869());
        parsers.add(new Stockist71050());
        parsers.add(new Stockist76679());
        parsers.add(new Stockist77580());
        parsers.add(new Stockist78442());
        parsers.add(new Stockist80261());
        parsers.add(new Stockist83071());
        parsers.add(new Stockist83072());
        parsers.add(new Stockist83220());
        parsers.add(new Stockist83367());
        parsers.add(new Stockist83825());
        parsers.add(new Stockist84208());
        parsers.add(new Stockist84212());
        parsers.add(new Stockist84281());
        parsers.add(new Stockist84595());
        parsers.add(new Stockist84632());
        parsers.add(new Stockist85365());
        parsers.add(new Stockist85661());
        parsers.add(new Stockist86822());
        parsers.add(new Stockist87420());
        parsers.add(new Stockist87890());
        parsers.add(new Stockist6728_1());
        parsers.add(new Stockist107831());
        parsers.add(new Stockist108007());
        parsers.add(new Stockist112085());
        parsers.add(new Stockist123463());
        parsers.add(new Stockist62724_1());
        parsers.add(new Stockist69055_Prudhavi());
        parsers.add(new Stockist6656_Maruthi());
        parsers.add(new Stockist62728_Laxmi_Anapurna());
        parsers.add(new Stockist6457());
        parsers.add(new Stockist62576());
        parsers.add(new Stockist62711());
        parsers.add(new Stockist15953());
        parsers.add(new Stockist7640());
        parsers.add(new Stockist6424());
        parsers.add(new Stockist83288());
        parsers.add(new Stockist7724());
        parsers.add(new Stockist121743());
        parsers.add(new Stockist62841());
        parsers.add(new Stockist77580());
        parsers.add(new Stockist121743());
        parsers.add(new Stockist52265830());
        */
        return parsers;
    }
}