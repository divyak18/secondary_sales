package com.squer.sales.parser.stockist;
import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;

import com.squer.sales.mail.MailLoggerEnum;
import com.squer.sales.mail.MailSystemPropertiesEnum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

public class Stockist84203 implements StatementParser {
    private boolean complete;
    private Double sales;
    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.ParserListener);
    private static final SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    @Override
    public boolean isValidStatement(String[] lines) throws Exception {

        if(lines.length < 10)
            return false;
        if(!lines[0].startsWith("PASBAAN MEDICAL AND GENERAL STORES")) {
            logger.info("The file is not for PASBAAN");
            return false;
        }
        System.out.println(lines[1]);

        if(!lines[1].startsWith("")) {
            logger.info("The file address is invalid");
            return false;
        }
        for(String line : lines){
            if (line.startsWith("From Date")) {
                checkStatementPeriod(line);
            }
            if (line.startsWith("Division Total")) {
                String numbers = line.substring("Division Total".length());
                StringTokenizer tokenizer = new StringTokenizer(numbers, " ");
                String value = tokenizer.nextToken().trim();
                this.sales = Double.parseDouble(value);
            }
        }
        return true;
    }

    private void checkStatementPeriod(String line) throws ParseException {
        int month = Integer.parseInt(SystemProperties.getProperty(MailSystemPropertiesEnum.StatementMonth));
        String fromDate = line.substring(line.indexOf(':') + 1, line.indexOf("To Date")).trim();
        String toDate = line.substring(line.lastIndexOf(':') + 1).trim();
        Date date = format.parse(fromDate);
        Calendar fromCalendar = Calendar.getInstance();
        fromCalendar.setTime(date);
        int fromMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (fromMonth != month) {
            complete = false;
            return;
        }
        date = format.parse(toDate);
        Calendar toCalendar = Calendar.getInstance();
        toCalendar.setTime(date);
        int toMonth = fromCalendar.get(Calendar.MONTH) + 1;
        if (toMonth != month) {
            complete = false;
            return;
        }
        complete = true;
    }

    @Override
    public boolean isComplete() {
        return complete;
    }

    @Override
    public Double getSales() {
        return sales;
    }

    @Override
    public String getStockistCode() {
        return "84203";
    }
   }
