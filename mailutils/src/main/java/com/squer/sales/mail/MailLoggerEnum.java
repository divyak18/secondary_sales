package com.squer.sales.mail;

import com.squer.platform.services.log.LoggerName;

public enum MailLoggerEnum implements LoggerName {
    MailListener("MailListener")
    ,ParserListener("ParserListener");

    private String loggerName;

    private MailLoggerEnum(String name){
        this.loggerName = name;
    }

    public String logger() {
        return loggerName;
    }
}