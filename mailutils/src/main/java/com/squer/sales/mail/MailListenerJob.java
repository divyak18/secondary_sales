package com.squer.sales.mail;

import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.security.AuthenticationHandler;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.sales.common.entity.MailAttachment;
import com.squer.sales.common.entity.MailLog;
import com.squer.sales.common.entity.MailStatus;
import com.squer.sales.common.entity.reference.MailLogReference;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import javax.mail.*;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

public class MailListenerJob implements Job{

    private SquerLogger logger = SquerLogger.getLogger(MailLoggerEnum.MailListener);
    private static String attachmentFolder = SystemProperties.getProperty(MailSystemPropertiesEnum.AttachmentSaveFolder);
    private static String folderPath =  attachmentFolder + File.separator + "wip" +  File.separator;

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.debug("Started the mail listener job");
        try{
            AuthenticationHandler.authenticateAsSchedulerAdmin();
            Repository repository = (Repository)ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            String host = SystemProperties.getProperty(MailSystemPropertiesEnum.MailHost);
            String port = SystemProperties.getProperty(MailSystemPropertiesEnum.MailPort);
            String tls = SystemProperties.getProperty(MailSystemPropertiesEnum.MailTLS);
            String user = SystemProperties.getProperty(MailSystemPropertiesEnum.MailUser);
            String password = SystemProperties.getProperty(MailSystemPropertiesEnum.MailPassword);

            Session session = Session.getDefaultInstance(new Properties( ));
            Store store = session.getStore("imaps");
            store.connect(host,
                    Integer.parseInt(port),
                    user,
                    password);
            Folder inbox = store.getFolder( "INBOX" );
            UIDFolder uf = (UIDFolder)inbox;
            inbox.open(Folder.READ_WRITE);

            Flags seen = new Flags(Flags.Flag.SEEN);
            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);

            // retrieve the messages from the folder in an array and print it
            Message[] messages = inbox.search(unseenFlagTerm); //emailFolder.getMessages();

            for (int i = 0, n = messages.length; i < n; i++) {
                Message message = messages[i];
                try {
                    Long messageId = uf.getUID(message);
                    logger.debug("Mail read:" + message.getSubject() + "--" + messageId + "---" + message.getReceivedDate());
                    MailLog log = new MailLog();
                    log.setMessageId(messageId);
                    log.setDateReceived(message.getReceivedDate());
                    log.setStatus(MailStatus.NEW_MAIL.getStatus());
                    MailLogReference reference = repository.create(log);
                    String subject = message.getSubject();
                    Address[] mailFrom = message.getFrom();
                    saveAttachment((MimeMultipart) message.getContent(), messageId, reference);
                    // Folder processedFolder = store.getFolder("Processed_" + processedDateFormat.format(new Date()) );
                    // moveMail(processedFolder, emailFolder, message);
                }catch(Exception e) {
                    logger.error("From:" + message.getFrom()[0].toString() + "-" + message.getSubject());
                    logger.error(e);
                    // String dateFormat = folderDateFormat.format(new Date());
                    // Folder dfolder = store.getFolder("Errors_" + dateFormat);
                    // moveMail(dfolder, emailFolder, message);
                }
            }

            //close the store and folder objects
            inbox.close(false);
            store.close();

        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }
        logger.info("Finished running the mail listener");
    }

    private String saveAttachment(
            MimeMultipart mimeMultipart,
            Long messageId,
            MailLogReference log)  throws MessagingException, IOException {
        String result = "";

        Repository repository = (Repository)ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        int count = mimeMultipart.getCount();
        logger.debug("Number of attachments in mail:" + messageId + "=" + count);
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            logger.debug("BodyPart:" + bodyPart.getDisposition());
            if (Part.INLINE.equalsIgnoreCase(bodyPart.getDisposition()) || Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
                String name = getAttachmentName(bodyPart, messageId);
                String wipPath = getWipPath(name);
                writeAttachmentToFile(wipPath, bodyPart);
                File baseFolder = new File(attachmentFolder + File.separator + "inputs");
                moveFileToInputFolder(wipPath, baseFolder, name);
                saveAttachmentLog(log, messageId,name, baseFolder, repository);
            }  else {
                if (bodyPart.isMimeType("text/plain")) {
                    result = result + "\n" + bodyPart.getContent();
                    //break; // without break same text appears twice in my tests
                } else if (bodyPart.isMimeType("text/html")) {
                    String html = (String) bodyPart.getContent();
                    result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    result = result + saveAttachment((MimeMultipart) bodyPart.getContent(), messageId, log);
                }
            }
        }
        return result;
    }

    private String getAttachmentName(BodyPart bodyPart, Long messageId) throws MessagingException {
        String name = "";
        String attachmentName = bodyPart.getFileName();
        if(attachmentName.indexOf('.')>0) {
            name = attachmentName.substring(0, attachmentName.indexOf('.')) + String.valueOf(messageId)
                    + attachmentName.substring(attachmentName.indexOf('.'));
        } else {
            name = attachmentName + messageId;
            String contentType = bodyPart.getContentType();
            if (contentType.indexOf("APPLICATION/VND.MS-EXCEL") >= 0) {
                name = name + ".xls";
            } else if (contentType.indexOf("APPLICATION/PDF") >= 0) {
                name = name + ".pdf";
            } else if (contentType.indexOf("IMAGE/JPEG") >= 0 ) {
                name = name +  ".jpg";
            } else {
                name = name + ".txt";
            }
        }
        return name;
    }

    private String getWipPath(String name) {
        return folderPath + File.separator + name;
    }

    private void writeAttachmentToFile(String wipPath, BodyPart bodyPart) throws MessagingException, IOException{
        FileOutputStream output = new FileOutputStream(wipPath);
        InputStream input = bodyPart.getInputStream();
        byte[] buffer = new byte[4096];
        int byteRead;
        while ((byteRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, byteRead);
        }
        output.close();
    }

    private void moveFileToInputFolder(String wipPath, File inputsFolder, String name) throws IOException{
        if (!inputsFolder.exists()) {
            inputsFolder.mkdir();
        }
        Files.move(new File(wipPath).toPath(), new File(inputsFolder + File.separator + name).toPath());
    }

    private void saveAttachmentLog(MailLogReference log, Long messageId, String name, File inputFolder, Repository repository ) throws IOException{
        try {
            MailAttachment attachment = new MailAttachment();
            attachment.setLog(log);
            attachment.setMessageId(messageId);
            attachment.setAttachmentName(name);
            attachment.setPath(inputFolder.getAbsolutePath());
            attachment.setStatus(MailStatus.NEW_ATTACHMENT.getStatus());
            repository.create(attachment);
        }catch (Exception e) {
            throw new IOException(e);
        }
    }
}