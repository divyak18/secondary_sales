package com.squer.sales.mail;

import com.squer.platform.services.util.SystemProperty;

public enum MailSystemPropertiesEnum implements SystemProperty {
    MailHost("mail.host")
    ,MailPort("mail.port")
    ,MailTLS("mail.tls")
    ,MailUser("mail.user")
    ,MailPassword("mail.password")
    ,RunMode("run.mode")
    ,StatementMonth("statement.month")
    ,InputFileName("input.fileName")
    ,AttachmentSaveFolder("attachment.folder");


    //public static SystemProperty StatementMonth;
    private String key;
    private MailSystemPropertiesEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}

/*
mail.host=IMAP.gmail.com
mail.port=993
mail.tls=true
mail.user=squersales@gmail.com
mail.password=Fbq567um

 */