package com.squer.platform.services.cache;

import java.io.Serializable;

/**
 * Created by ashutoshpavaskar on 18/09/16.
 */
public class Cacheable implements Serializable {

    private Object cached;

    public Cacheable(Object o){
        this.cached = o;
    }

    public Object getCached() {
        return cached;
    }
}
