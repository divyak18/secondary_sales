package com.squer.platform.services.exception;

/**
 * Created by ashutoshpavaskar on 05/05/17.
 */
public enum CoreExceptionEnum implements ExceptionMeta {

    UNKNOWN_EXCEPTION("500","An unknown error has ocurred. Please contact your system administrator/support","ERROR")
    ,INVALID_CREDENTIALS("500","Invalid login credentials","INFO")
    ,INVALID_REFERENCE("500","Invalid reference password", "ERROR")
    ;

    private String severity;
    private String message;
    private String code;

    private CoreExceptionEnum(String code, String message, String severity) {
        this.severity = severity;
        this.code = code;
        this.message = message;
    }

    @Override
    public String getSeverity() {
        return severity;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getCode() {
        return code;
    }
}
