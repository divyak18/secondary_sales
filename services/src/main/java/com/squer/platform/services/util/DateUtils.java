package com.squer.platform.services.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    private  static final DateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static Date convertStringToDate(String dateString) throws Exception {
        if (dateString == null || dateString.equalsIgnoreCase(""))
            return  null;

        return inputDateFormat.parse(dateString);
    }

    public static Date convertStringToDate(String dateString, String format) throws Exception {
        if (dateString == null || dateString.equalsIgnoreCase(""))
            return  null;
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.parse(dateString);
    }

}
