package com.squer.platform.services.util;

import javax.crypto.spec.SecretKeySpec;
import java.rmi.server.UID;
import java.security.Key;
import java.security.MessageDigest;
import java.util.Date;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;
import org.jasypt.util.text.BasicTextEncryptor;


/**;
 * Created with IntelliJ IDEA.;
 * User: ashutoshpavaskar;
 * Date: 22/08/13;
 * Time: 8:41 PM;
 * To change this template use File | Settings | File Templates.;
 */
public class StringUtil {
    private static final String ALGO = "PBEWithMD5AndTripleDES";
    private static final byte[] keyValue ="SQUER_CERTIFICATE".getBytes();
    private static    char[] values = {'a','b','c','d','e','f','g','h','i','j',
            'k','l','m','n','o','p','q','r','s','t',
            'u','v','w','x','y','z','0','1','2','3',
            '4','5','6','7','8','9'};
    private static BasicTextEncryptor encryptor = null;
    static {
        encryptor = new BasicTextEncryptor();
        encryptor.setPassword("SQUER");
    }

    public static String encrypt(String data) throws Exception{
        /**
         Key key = generateKey();
         Cipher c = Cipher.getInstance(ALGO);
         c.init(Cipher.ENCRYPT_MODE, key);
         byte[] encVal = c.doFinal(Data.getBytes("UTF-8"));
         return new String(Hex.encodeHex(encVal));
         */
        return encryptor.encrypt(data);
    }

    public static String getHash(String value) throws Exception{
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        digest.reset();
        digest.update(value.getBytes("UTF-8"));
        byte[] shaDig = digest.digest();
        return new String(Hex.encodeHex(shaDig));
    }
    public static String decrypt(String encryptedData) throws Exception{
        /**
         Key key = generateKey();
         Cipher c = Cipher.getInstance(ALGO);
         c.init(Cipher.DECRYPT_MODE, key);
         byte[] decValue = c.doFinal(Hex.decodeHex(encryptedData.toCharArray()));
         return new String(decValue);
         */
        return  encryptor.decrypt(encryptedData);
    }


    public static String generateRandomString(int length) {
        Random random = new Random((new Date()).getTime());
        String out = "";
        for (int i=0;i<length;i++) {
            int idx=random.nextInt(values.length);
            out += values[idx];
        }
        return out;
    }


    private static Key generateKey() throws Exception{
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }

    public static String getUID(){
        String uidString = ("00000000000000000000000000000000" + new UID().toString());
        uidString = uidString.substring(uidString.length() - 32);
        uidString = uidString.replace(':', '0');
        uidString = uidString.replace('-', '0');
        return uidString;
    }

    public static boolean isNullOrEmpty(String value){
        return value==null || "".equals(value);
    }



}
