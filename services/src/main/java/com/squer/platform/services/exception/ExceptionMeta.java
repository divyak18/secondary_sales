package com.squer.platform.services.exception;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface ExceptionMeta {

    public String getSeverity();

    public String getMessage();

    public String getCode();

}
