package com.squer.platform.services.entity.impl;

import com.squer.platform.services.entity.SquerReference;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class SquerReferenceImpl implements SquerReference {

    private String id;

    public SquerReferenceImpl(){

    }

    public SquerReferenceImpl(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
