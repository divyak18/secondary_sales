package com.squer.platform.services.cache;

/**
 * Created by ashutoshpavaskar on 18/09/16.
 */
public interface CacheIdentifier {

    public String getName();
}
