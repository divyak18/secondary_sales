package com.squer.platform.services.exception;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class SquerException extends Exception {
    private ExceptionMeta meta = null;

    public SquerException(ExceptionMeta meta){
        this.meta = meta;
        initCause(new Exception(meta.getMessage()));
    }

    public SquerException(Exception e){
        this.meta = CoreExceptionEnum.UNKNOWN_EXCEPTION;
        initCause(e);
    }

    public String getSeverity(){
        return meta.getSeverity();
    }

    public ExceptionMeta getMeta() {
        return meta;
    }
}
