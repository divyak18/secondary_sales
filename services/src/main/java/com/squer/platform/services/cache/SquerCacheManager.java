package com.squer.platform.services.cache;

import com.squer.platform.services.log.LoggerName;
import com.squer.platform.services.log.SquerLogger;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.Configuration;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.xml.XmlConfiguration;

import java.net.URL;

/**
 * Created by ashutoshpavaskar on 18/09/16.
 */
public class SquerCacheManager {
    private  final URL myUrl = this.getClass().getResource("/config/cacheconfig.xml");
    private CacheManager cacheManager = null;
    private static SquerCacheManager manager  = new SquerCacheManager();
    private SquerLogger logger = SquerLogger.getLogger(LoggerName.PlatformLogger.CacheLogger);

    private SquerCacheManager(){
        Configuration xmlConfig = new XmlConfiguration(myUrl);
        cacheManager = CacheManagerBuilder.newCacheManager(xmlConfig);
        cacheManager.init();
    }

    public static SquerCacheManager getInstance(){
        return manager;
    }

    public void put(CacheIdentifier identifier, String key, Object o){
        Cacheable c = new Cacheable(o);
        Cache<String, Cacheable> basicCache = cacheManager.getCache(identifier.getName(), String.class, Cacheable.class);
        basicCache.put(key, c);
        logger.debug("Adding to Cache:" + identifier.getName() + "===>" + key  +"=" + c);
    }

    public Object get(CacheIdentifier identifier, String key){
        Cache<String, Cacheable> basicCache = cacheManager.getCache(identifier.getName(), String.class, Cacheable.class);
        Cacheable c = basicCache.get(key);
        logger.debug("Getting from Cache:" + identifier.getName() + "===>" + key + "=" + c);
        if(c!=null)
            return c.getCached();
        return null;
    }


}
