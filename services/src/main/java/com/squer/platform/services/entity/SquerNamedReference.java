package com.squer.platform.services.entity;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface SquerNamedReference extends SquerReference {

    public String getName();
}
