package com.squer.platform.services.util;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface SystemProperty {

    public String getKey();
}
