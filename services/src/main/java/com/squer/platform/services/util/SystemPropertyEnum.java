package com.squer.platform.services.util;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public enum SystemPropertyEnum implements  SystemProperty{
    CONFIG_PATH("config.path"),
    DEFAULT_PASSWORD("default.password"),
    UPLOAD_PATH("upload.folder"),
    EXCEPTION_META_FOLDER("exception_meta_folder"),
    SEARCH_NUM_ROWS("search_num_rows"),
    META_FOLDER("meta_folder");

    private String key;

    private SystemPropertyEnum(String key){
        this.key = key;
    }

    public String getKey(){
        return key;
    }
}
