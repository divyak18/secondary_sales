package com.squer.platform.services.entity.impl;

import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.Auditable;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;

import java.util.Date;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public abstract class AuditableImpl implements SquerEntity, Auditable {
    @Attribute(column = "id", dbType="ID", mapped = "reference.id")
    private SquerReference reference;

    @Attribute(column = "created_on", dbType = "T_STAMP")
    private Date createdOn;

    @Attribute(column = "updated_on", dbType = "T_STAMP")
    private Date updatedOn;

    @Attribute(column = "created_by", dbType="ID")
    private String createdBy;

    @Attribute(column = "updated_by", dbType="ID")
    private String updatedBy;

    @Attribute(column = "stale_id")
    private long staleId;


    @Override
    public SquerReference getReference() {
        return reference;
    }

    @Override
    public void setReference(SquerReference reference) {
        this.reference = reference;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public long getStaleId() {
        return staleId;
    }

    public void setStaleId(long staleId) {
        this.staleId = staleId;
    }
}
