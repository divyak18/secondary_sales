package com.squer.platform.services.entity;

import java.util.Date;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface Auditable {
    public Date getCreatedOn();
    public void setCreatedOn(Date createdOn);

    public Date getUpdatedOn();
    public void setUpdatedOn(Date updatedOn);

    public String getCreatedBy();
    public void setCreatedBy(String createdBy);

    public String getUpdatedBy();
    public void setUpdatedBy(String updatedBy);

    public long getStaleId();
    public void setStaleId(long staleId);
}
