package com.squer.platform.services.entity.impl;

import com.squer.platform.services.entity.SquerNamedReference;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class SquerNamedReferenceImpl extends SquerReferenceImpl implements SquerNamedReference{
    private String name;

    public SquerNamedReferenceImpl(){

    }

    public SquerNamedReferenceImpl(String id, String name){
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
