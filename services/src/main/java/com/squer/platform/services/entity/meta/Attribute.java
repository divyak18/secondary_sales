package com.squer.platform.services.entity.meta;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by ashutoshpavaskar on 29/11/16.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Attribute {
    String column() default "";
    String prefix() default "";
    String since() default "1.0";
    String dbType() default "";
    String mapped() default "";
}
