package com.squer.platform.services.log;

/**
 * Created by ashutoshpavaskar on 17/09/16.
 */
public interface LoggerName {

    public String logger();

    /**
     * Created by ashutoshpavaskar on 18/09/16.
     */
    enum PlatformLogger implements LoggerName{
        CacheLogger("Cache");

        private String name;
        private PlatformLogger(String name){
            this.name = name;
        }


        public String logger() {
            return name;
        }
    }
}
