package com.squer.platform.services.entity.meta;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by ashutoshpavaskar on 29/11/16.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Entity {
    String since() default "1.0";
    String table();
    String reference() default "";
    String module();
    String prefix();
    String model() default "BaseModel";
}
