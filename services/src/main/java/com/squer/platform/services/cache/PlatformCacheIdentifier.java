package com.squer.platform.services.cache;

/**
 * Created by ashutoshpavaskar on 18/09/16.
 */
public enum PlatformCacheIdentifier implements CacheIdentifier {
    PERSISTENCE("persistence");

    private String name;

    private PlatformCacheIdentifier(String name){
        this.name = name;
    }


    public String getName() {
        return name;
    }
}
