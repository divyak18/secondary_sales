package com.squer.platform.services.util;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class SystemProperties {


    private static String PATH_SEPARATOR =  System.getProperty("file.separator");
    private static Map<String, String> propertyMap = new HashMap<>();

    private static String getProperty(String name){
        String configPath = System.getProperty(SystemPropertyEnum.CONFIG_PATH.getKey());
        propertyMap.put(SystemPropertyEnum.CONFIG_PATH.getKey(), configPath);
        InputStream input = null;
        Properties properties = new Properties();
        if(properties.size()>0) {
            return getProperty( propertyMap, name);
        }

        try {
            input = new FileInputStream(new File(configPath + PATH_SEPARATOR + "config.properties"));
            properties.load(input);
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                propertyMap.put(entry.getKey().toString(), entry.getValue().toString());
            }
            return  getProperty(propertyMap, name);

        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }



    public static String getProperty(SystemProperty property){
        return getProperty(property.getKey());
    }


    private static String getProperty(Map<String, String> properties, String name) {
            if(properties.containsKey(name))
                return properties.get(name);
            throw new RuntimeException("Unknown key:" + name);
    }

}
