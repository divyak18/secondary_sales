package com.squer.platform.services.entity;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface NamedEntity {

    public void setName(String name);
    public String getName();

    public void setCiName(String name);
    public String getCiName();

}
