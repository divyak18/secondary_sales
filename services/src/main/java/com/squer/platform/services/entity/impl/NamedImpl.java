package com.squer.platform.services.entity.impl;

import com.squer.platform.services.entity.NamedEntity;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerNamedReference;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Attribute;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public abstract class NamedImpl implements SquerEntity, NamedEntity {
    @Attribute(column = "id", dbType="ID", mapped = "reference.id")
    private SquerNamedReferenceImpl reference;

    @Attribute(column = "name", dbType="L_STRING")
    private String name;

    @Attribute(column = "ci_name", dbType="L_STRING")
    private String ciName;

    public SquerNamedReference getReference() {
        return reference;
    }

    public void setReference(SquerReference reference) {
        this.reference = (SquerNamedReferenceImpl) reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if(name!=null)
            setCiName(name.toLowerCase());
    }

    public String getCiName() {
        return ciName;
    }

    public void setCiName(String ciName) {
        this.ciName = ciName;
    }
}
