package com.squer.platform.services.entity.impl;

import com.squer.platform.services.entity.*;
import com.squer.platform.services.entity.meta.Attribute;

import java.util.Date;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public abstract class StandardEntity implements SquerEntity, NamedEntity, Auditable {

    @Attribute(column = "id", dbType="ID", mapped = "reference.id")
    private SquerNamedReference reference;

    @Attribute(column = "name", dbType="L_STRING")
    private String name;

    @Attribute(column = "ci_name", dbType="L_STRING")
    private String ciName;

    @Attribute(column = "created_on", dbType = "T_STAMP")
    private Date createdOn;

    @Attribute(column = "updated_on", dbType = "T_STAMP")
    private Date updatedOn;

    @Attribute(column = "created_by", dbType = "ID")
    private String createdBy;

    @Attribute(column = "updated_by", dbType = "ID")
    private String updatedBy;

    @Attribute(column = "stale_id", dbType = "LONG")
    private long staleId;

    @Override
    public SquerNamedReference getReference() {
        return reference;
    }

    @Override
    public void setReference(SquerReference reference) {
        this.reference = (SquerNamedReference) reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if(name!=null)
            setCiName(name.toLowerCase());
    }

    public String getCiName() {
        return ciName;
    }

    public void setCiName(String ciName) {
        this.ciName = ciName;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public long getStaleId() {
        return staleId;
    }

    public void setStaleId(long staleId) {
        this.staleId = staleId;
    }
}
