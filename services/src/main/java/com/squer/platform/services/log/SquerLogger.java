package com.squer.platform.services.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 17/09/16.
 */
public class SquerLogger {

    private static Map<String, SquerLogger> logs = new HashMap<>();
    private Logger logger = null;
    private SquerLogger(LoggerName name){
        logger = LoggerFactory.getLogger(name.logger());
        logs.put(name.logger(), this);
    }

    public static SquerLogger getLogger(LoggerName loggerEntry){
        if(logs.containsKey(loggerEntry.logger()))
            return logs.get(loggerEntry.logger());
        return new SquerLogger(loggerEntry);
    }

    public void debug(String s){
        logger.debug(s);
    }


    public void error(String s){
        logger.error(s);
    }


    public void error(Throwable t){
        logger.error("Error Thrown!!!!!!!!!!!!!!!!!!!!!!!!!!!",t);
    }

    public void info(String s){
        logger.info(s);
    }

    public void warn(String s){
        logger.warn(s);
    }

    public void warn(Throwable t){
        logger.warn("Warning thrown!!!!!!!", t);
    }


}
