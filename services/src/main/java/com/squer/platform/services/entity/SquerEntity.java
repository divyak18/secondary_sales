package com.squer.platform.services.entity;


/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface SquerEntity{

    public SquerReference getReference();

    public void setReference(SquerReference reference);

}
