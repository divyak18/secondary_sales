package com.squer.test.platform.cache;


import com.squer.platform.services.cache.CacheIdentifier;

/**
 * Created by ashutoshpavaskar on 18/09/16.
 */
public enum TestCacheIdentifier implements CacheIdentifier {
    Finder_Cache("finderQueries");


    private String name;

    private TestCacheIdentifier(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
