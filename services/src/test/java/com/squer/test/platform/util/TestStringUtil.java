package com.squer.test.platform.util;

import com.squer.platform.services.util.StringUtil;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by ashutoshpavaskar on 08/12/16.
 */
public class TestStringUtil {

    @Test(groups = {"platform"})
    public void testHash(){
        try{
            String encrypted = StringUtil.getHash("welc0m3");
            System.out.println(encrypted);
        }catch(Exception e){
            Assert.fail(e.getMessage(), e);
        }
    }

    @Test(groups = {"platform"})
    public void testId(){
        try{
            String id = StringUtil.getUID();
            System.out.println(id);
            Assert.assertNotNull(id);
            Assert.assertEquals(id.length(),32);
        }catch (Exception e){
            Assert.fail(e.getMessage(),e);
        }
    }
}
