package com.squer.test.platform.log;

import com.squer.platform.services.log.SquerLogger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by ashutoshpavaskar on 17/09/16.
 */
public class SquerLoggerTest {

    @BeforeClass
    public void setUp() {
        // code that will be invoked when this test is instantiated
    }

    @Test(groups = { "platform" })
    public void debug() {
        SquerLogger logger = SquerLogger.getLogger(TestLogger.TEST_LOGGER_DEBUG);
        logger.debug("Hello World");
    }

    @Test(groups = { "platform" })
    public void error() {
        SquerLogger logger = SquerLogger.getLogger(TestLogger.TEST_LOGGER_ERROR);
        logger.error("Hello Error");
        logger.debug("Hello Debug in error");
        logger.error(new RuntimeException("Here is a runtime exception"));
    }



}
