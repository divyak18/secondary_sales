package com.squer.test.platform.log;


import com.squer.platform.services.log.LoggerName;

/**
 * Created by ashutoshpavaskar on 17/09/16.
 */
public enum TestLogger implements LoggerName {

     TEST_LOGGER_DEBUG("Test_logger_d"),
     TEST_LOGGER_ERROR("Test_logger_e"),
     TEST_LOGGER_FATAL("Test_logger_f")
    ;

    private String name;

    private TestLogger(String name){
        this.name = name;
    }


    public String logger() {
        return name;
    }
}
