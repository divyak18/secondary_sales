package com.squer.test.platform.cache;

import com.squer.platform.services.cache.SquerCacheManager;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by ashutoshpavaskar on 18/09/16.
 */
public class SquerCacheManagerTest {

    public void putTest(){
        try {
            SquerCacheManager manager = SquerCacheManager.getInstance();
            manager.put(TestCacheIdentifier.Finder_Cache, "Hello", "Welcome");
            String val = (String) manager.get(TestCacheIdentifier.Finder_Cache, "Hello");
            System.out.println(val);
            Assert.assertEquals(val, "Welcome");
        }catch (Exception e){
            Assert.fail("Cache test failed", e);
        }
    }

}
