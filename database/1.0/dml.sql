delete from fmk_app_config;
insert into fmk_app_config(ID, app_version, admin_password ) values(
  'appcg0000003d34b5ab0158dc6b7c4f008000',
  '1.0',
  '280d44ab1e9f79b5cce2dd4f58f5fe91f0fbacdac9f7447dffc318ceb79f2d02');


delete from FMK_SYSTEM_LOV where lov_type = 'ENTITY_STATUS';
insert into fmk_system_lov(display, id, lov_type, name, ci_name, display_order)
  values(false, 'SYSLV00000000000000000000000000000006','ENTITY_STATUS','Active',  'active', 1);
insert into fmk_system_lov(display, id, lov_type, name, ci_name, display_order)
  values(false, 'SYSLV00000000000000000000000000000007','ENTITY_STATUS','InActive',  'inactive', 2);

delete from sch_job_definition;
insert into sch_job_definition(name, updated_on, stale_id, ci_name, created_on, id, exec_class, updated_by, created_by)
    values('Mail Listener', current_date, 1, ',mail listener', current_date, 'SCHJB00000000000000000000000000000001', 'com.squer.sales.mail.MailListenerJob','USERS00000000000000000000000000000001', 'USERS00000000000000000000000000000001');
insert into sch_job_definition(name, updated_on, stale_id, ci_name, created_on, id, exec_class, updated_by, created_by)
    values('File Listener', current_date, 1, ',mail listener', current_date, 'SCHJB00000000000000000000000000000002', 'com.squer.sales.parser.FileReaderJob','USERS00000000000000000000000000000001', 'USERS00000000000000000000000000000001');
/*insert into sch_job_definition(name, updated_on, stale_id, ci_name, created_on, id, exec_class, updated_by, created_by)
    values('File Listener', current_date, 1, ',mail listener', current_date, 'SCHJB00000000000000000000000000000003', 'com.squer.sales.parser.FileReaderJobTest','USERS00000000000000000000000000000001', 'USERS00000000000000000000000000000001');
*/

delete from sch_schedule_definition;
insert into sch_schedule_definition(group_name, id, created_by, start_time, start_date, stale_id, status,
                                    job_id, updated_by, frequency_unit, end_date, updated_on, created_on, schedule_frequency)
    values('MailListener', 'SCHDR00000000000000000000000000000001', 'USERS00000000000000000000000000000001','10:00', current_date, 1, 'NEW',
           'SCHJB00000000000000000000000000000001', 'USERS00000000000000000000000000000001', 'MINUTES', null, current_date, current_date, 30);
insert into sch_schedule_definition(group_name, id, created_by, start_time, start_date, stale_id, status,
                                    job_id, updated_by, frequency_unit, end_date, updated_on, created_on, schedule_frequency)
    values('FileListener', 'SCHDR00000000000000000000000000000002', 'USERS00000000000000000000000000000001','10:00', current_date, 1, 'NEW',
                       'SCHJB00000000000000000000000000000002', 'USERS00000000000000000000000000000001', 'MINUTES', null, current_date, current_date, 5);


/*insert into sch_schedule_definition(group_name, id, created_by, start_time, start_date, stale_id, status,
                                    job_id, updated_by, frequency_unit, end_date, updated_on, created_on, schedule_frequency)
    values('FileListener', 'SCHDR00000000000000000000000000000003', 'USERS00000000000000000000000000000001','10:00', current_date, 1, 'NEW',
                       'SCHJB00000000000000000000000000000003', 'USERS00000000000000000000000000000001', 'MINUTES', null, current_date, current_date, 5);


update sch_job_definition set exec_class='com.squer.sales.parser.FileReaderJobTest' where id='SCHJB00000000000000000000000000000001';*/