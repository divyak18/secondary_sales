DROP TABLE IF EXISTS CMT_MAIL_ATTACHMENT;
CREATE TABLE CMT_MAIL_ATTACHMENT (
    AMOUNT NUMERIC(10,2) ,
    id CHAR(37) ,
    created_by CHAR(37) ,
    MAIL_LOG_ID CHAR(37) ,
    STOCKIST_CODE VARCHAR(25) ,
    MESSAGE_ID BIGINT ,
    stale_id VARCHAR(255) ,
    updated_by CHAR(37) ,
    ATTACHMENT_PATH VARCHAR(255) ,
    ATTACHMENT_NAME VARCHAR(255) ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    created_on TIMESTAMP WITH TIME ZONE ,
    STATUS VARCHAR(100) );
DROP TABLE IF EXISTS SCH_SCHEDULE_DEFINITION;
CREATE TABLE SCH_SCHEDULE_DEFINITION (
    group_name VARCHAR(100) ,
    id CHAR(37) ,
    created_by CHAR(37) ,
    start_time VARCHAR(25) ,
    start_date DATE ,
    status VARCHAR(100) ,
    stale_id VARCHAR(255) ,
    job_id CHAR(37) ,
    updated_by CHAR(37) ,
    frequency_unit VARCHAR(25) ,
    end_date DATE ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    created_on TIMESTAMP WITH TIME ZONE ,
    schedule_frequency BIGINT );
DROP TABLE IF EXISTS FMK_SECURITY_ROLE;
CREATE TABLE FMK_SECURITY_ROLE (
    name VARCHAR(255) ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    stale_id BIGINT ,
    ci_name VARCHAR(255) ,
    created_on TIMESTAMP WITH TIME ZONE ,
    id CHAR(37) ,
    updated_by CHAR(37) ,
    created_by CHAR(37) );
DROP TABLE IF EXISTS FMK_APP_CONFIG;
CREATE TABLE FMK_APP_CONFIG (
    admin_password VARCHAR(255) ,
    app_version VARCHAR(255) ,
    id CHAR(37) );
DROP TABLE IF EXISTS CMT_MAIL_LOG;
CREATE TABLE CMT_MAIL_LOG (
    id CHAR(37) ,
    updated_by CHAR(37) ,
    MESSAGE_ID BIGINT ,
    created_by CHAR(37) ,
    STATUS VARCHAR(100) ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    created_on TIMESTAMP WITH TIME ZONE ,
    stale_id VARCHAR(255) ,
    DATE_RECEIVED TIMESTAMP );
DROP TABLE IF EXISTS SCH_JOB_DEFINITION;
CREATE TABLE SCH_JOB_DEFINITION (
    name VARCHAR(255) ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    stale_id BIGINT ,
    ci_name VARCHAR(255) ,
    created_on TIMESTAMP WITH TIME ZONE ,
    id CHAR(37) ,
    exec_class VARCHAR(255) ,
    updated_by CHAR(37) ,
    created_by CHAR(37) );
DROP TABLE IF EXISTS FMK_SECURITY_PRIVILEGE;
CREATE TABLE FMK_SECURITY_PRIVILEGE (
    ID CHAR(37) ,
    PRIV_DESCRIPTION TEXT ,
    PRIV_NAME VARCHAR(255) );
DROP TABLE IF EXISTS FMK_USER_LOV;
CREATE TABLE FMK_USER_LOV (
    name VARCHAR(255) ,
    lov_type VARCHAR(100) ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    stale_id BIGINT ,
    ci_name VARCHAR(255) ,
    created_on TIMESTAMP WITH TIME ZONE ,
    display_order INT ,
    id CHAR(37) ,
    display BOOLEAN ,
    updated_by CHAR(37) ,
    created_by CHAR(37) );
DROP TABLE IF EXISTS FMK_COUNTER;
CREATE TABLE FMK_COUNTER (
    count SERIAL ,
    id CHAR(37) ,
    owner_id VARCHAR(100) );
DROP TABLE IF EXISTS FMK_SECURITY_USER;
CREATE TABLE FMK_SECURITY_USER (
    password VARCHAR(255) ,
    id CHAR(37) ,
    updated_by CHAR(37) ,
    owner_id CHAR(37) ,
    created_by CHAR(37) ,
    updated_on TIMESTAMP WITH TIME ZONE ,
    created_on TIMESTAMP WITH TIME ZONE ,
    stale_id VARCHAR(255) ,
    status_id CHAR(37) ,
    user_name VARCHAR(100) );
DROP TABLE IF EXISTS FMK_SYSTEM_LOV;
CREATE TABLE FMK_SYSTEM_LOV (
    ci_name VARCHAR(255) ,
    display BOOLEAN ,
    name VARCHAR(255) ,
    lov_type VARCHAR(100) ,
    id CHAR(37) ,
    display_order INT );
