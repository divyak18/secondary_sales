#!/usr/bin/env bash
export CODE_PATH=/Users/ashutoshpavaskar/codebase/repository/pocketpower/api
export MAVEN_HOME=/Users/ashutoshpavaskar/codebase/libraries/apache-maven-3.3.9/
export PATH=$PATH:$MAVEN_HOME/bin
export CLASSPATH=$CLASSPATH:$CODE_PATH/config
export JAVA_OPTS="-Dconfig.path=$CODE_PATH/config"
export TOMCAT_PATH=/Users/ashutoshpavaskar/codebase/tomcat/tomcat/
echo $1

mvn -f ../$1/pom.xml clean
mvn -f ../$1/pom.xml install -Dmaven.test.skip=true
rc=$?
if [[ $rc -ne 0 ]] ; then
    echo "*****************************************************"
    echo "Failed to build " $i
    echo "*****************************************************"
    exit
fi

mvn -f ../codegenerator/pom.xml -Dinstall.path=$CODE_PATH exec:java -Dexec.mainClass=com.squer.tools.codegen.generators.GeneratorEngine
mvn -f ../web/pom.xml clean
mvn -f ../web/pom.xml install -Dmaven.test.skip=true

rm -rf $TOMCAT_PATH/webapps/pocketpower*
cp $CODE_PATH/config/setenv.sh $TOMCAT_PATH/bin/setenv.sh
cp $CODE_PATH/web/target/web-1.0-SNAPSHOT.war $TOMCAT_PATH/webapps/pocketpower.war
