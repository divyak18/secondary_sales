#!/bin/sh
export USER_HOME=/home/divya
# export TOMCAT_HOME=$USER_HOME/codebase/tomcat//tomcat
export WEB_LIB=/home/divya/secondary_sales_new/secondary_sales/build/target/lib
export CLASSPATH=''
export WEB_CLASSPATH=/home/divya/secondary_sales_new/secondary_sales/config

echo $WEB_LIB
for i in $WEB_LIB/*.jar; do
    WEB_CLASSPATH=$WEB_CLASSPATH:$i
done


CLASSPATH=`echo $CLASSPATH | cut -c2-`
echo '***************************************'
echo $WEB_CLASSPATH
echo '***************************************'

export JAVA_OPTS="-Drun.mode=test -Dconfig.path=/home/divya/secondary_sales_new/secondary_sales/config"
echo $JAVA_OPTS

export CLASSPATH=$CLASSPATH:/home/divya/secondary_sales_new/secondary_sales/config/logback.xml:$WEB_CLASSPATH

java $JAVA_OPTS "-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=8001" -cp .:$WEB_CLASSPATH com.squer.platform.scheduler.textui.SchedulerEngine