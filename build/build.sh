##!/usr/bin/env bash
#export CODE_PATH=/Users/ashutoshpavaskar/codebase/repository/timesheet/api
#export MAVEN_HOME=/Users/ashutoshpavaskar/codebase/libraries/apache-maven-3.3.9/
#export PATH=$PATH:$MAVEN_HOME/bin
#export CLASSPATH=$CLASSPATH:$CODE_PATH/config
##export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_144.jdk/Contents/Home
#export JAVA_OPTS="-Dconfig.path=$CODE_PATH/config"
#export TOMCAT_PATH=/Users/ashutoshpavaskar/codebase/tomcat/tomcat/
#
#export SKIP_TESTS="1"
#export RUN_TESTS="1"
#export RUN_DB="1"
#
#modules=(services core scheduler common timesheet uiframework client  tools)
#testmodules=(services core scheduler common timesheet uiframework client  tools)
#
##modules=(services core scheduler common pocketpower uiframework client  tools)
##testmodules=(services core scheduler common  pocketpower uiframework client  tools)
#
#
#if [ "$SKIP_TESTS" = "1" ]
#then
#for i in "${modules[@]}"
#do
#:
#    mvn -f ../$i/pom.xml clean
#    mvn -f ../$i/pom.xml install -Dmaven.test.skip=true
#    rc=$?
#    if [[ $rc -ne 0 ]] ; then
#        echo "*****************************************************"
#        echo "Failed to build " $i
#        echo "*****************************************************"
#        exit
#    fi
#done
#fi
#
#mvn -f ../codegenerator/pom.xml -Dinstall.path=$CODE_PATH exec:java -Dexec.mainClass=com.squer.tools.codegen.generators.GeneratorEngine
#if [ "$RUN_DB" = "1" ]
#then
#    mvn -f ../codegenerator/pom.xml -Dinstall.path=$CODE_PATH exec:java -Dexec.mainClass=com.squer.tools.codegen.generators.DbRunner
#fi
#
#mvn -f ../web/pom.xml clean
#mvn -f ../web/pom.xml install -Dmaven.test.skip=true
#
#
#if [ "$RUN_TESTS" = "1" ]
#then
#for i in "${testmodules[@]}"
#do
#:
#    mvn -f ../$i/pom.xml test
#done
#fi
#
##run tests
#echo "Start deployment"
#rm -rf $TOMCAT_PATH/webapps/timesheet*
#cp $CODE_PATH/config/setenv.sh $TOMCAT_PATH/bin/setenv.sh
#cp $CODE_PATH/web/target/web-1.0-SNAPSHOT.war $TOMCAT_PATH/webapps/timesheet.war
#


#!/usr/bin/env bash
export CODE_PATH=/home/divya/secondary_sales_new/secondary_sales
export MAVEN_HOME=/home/divya/Desktop/secondarysales/apache-maven-3.5.3/
export PATH=$PATH:$MAVEN_HOME/bin
export CLASSPATH=$CLASSPATH:$CODE_PATH/config
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
export JAVA_OPTS="-Dconfig.path=$CODE_PATH/config"
export TOMCAT_PATH=/home/divya/Desktop/secondarysales/tomcat/

export SKIP_TESTS="1"
export RUN_TESTS="0"
export RUN_DB="1"

modules=(services core scheduler common mailutils uiframework client  tools)
testmodules=(services core scheduler common mailutils uiframework client  tools)



if [ "$SKIP_TESTS" = "1" ]
then
for i in "${modules[@]}"
do
:
    mvn -f ../$i/pom.xml clean
    mvn -f ../$i/pom.xml install -Dmaven.test.skip=true
    rc=$?
    if [[ $rc -ne 0 ]] ; then
        echo "*****************************************************"
        echo "Failed to build " $i
        echo "*****************************************************"
        exit
    fi
done
fi

mvn -f ../codegenerator/pom.xml -Dinstall.path=$CODE_PATH exec:java -Dexec.mainClass=com.squer.tools.codegen.generators.GeneratorEngine
if [ "$RUN_DB" = "0" ]
then
   mvn -f ../codegenerator/pom.xml -Dinstall.path=$CODE_PATH exec:java -Dexec.mainClass=com.squer.tools.codegen.generators.DbRunner
fi

mvn -f ../build/pom.xml clean
mvn -f ../build/pom.xml install -Dmaven.test.skip=true


if [ "$RUN_TESTS" = "0" ]
then
for i in "${testmodules[@]}"
do
:
    mvn -f ../$i/pom.xml test
done
fi

#run tests
echo "Start deployment"
rm -rf $TOMCAT_PATH/webapps/sales*
cp $CODE_PATH/config/setenv.sh $TOMCAT_PATH/bin/setenv.sh
cp $CODE_PATH/web/target/web-1.0-SNAPSHOT.war $TOMCAT_PATH/webapps/sales.war
echo "war deployed successfullyyyyy!!!!!"



