package com.squer.core.registry;

import com.squer.platform.services.util.SystemProperty;

public enum CoreSystemProperties implements SystemProperty {
    MongoHost("mongodb.host")
    ,MongoPort("mongodb.port")
    ,MongoDb("mongodb.name");

    private String key;
    private CoreSystemProperties(String key) {
        this.key = key;
    }

    @Override
    public String getKey() {
        return key;
    }
}
