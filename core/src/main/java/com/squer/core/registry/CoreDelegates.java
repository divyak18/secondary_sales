package com.squer.core.registry;

import com.squer.platform.persistence.Delegates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 17/05/17.
 */
public class CoreDelegates implements Delegates {


    private static Map<String, Delegates> delegatesMap = new HashMap<>();

    public static final Delegates UserEntityModel = new CoreDelegates("UserEntityModel");


    private String id;

    private CoreDelegates(){

    }

    private CoreDelegates(String id){
        this.id = id;
        delegatesMap.put(id, this);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public Delegates getDelegate(String id) {
        return delegatesMap.get(id);
    }
}
