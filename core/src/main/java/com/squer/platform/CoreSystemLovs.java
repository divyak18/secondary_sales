package com.squer.platform;

import com.squer.platform.core.reference.SystemLovReference;
import com.squer.platform.security.ServiceLocator;

/**
 * Created by ashutoshpavaskar on 28/12/16.
 */
public enum CoreSystemLovs implements SystemLovConstants {

    ACTIVE_STATUS("SYSLV00000000000000000000000000000006", "Active", "STATUS", 1, true),
    INACTIVE_STATUS("SYSLV00000000000000000000000000000007", "In Active", "STATUS", 2, true),
    ;

    private String id;
    private String name;
    private String type;
    private int displayOrder;
    private boolean display;

    private CoreSystemLovs(String id,
                           String name,
                           String type,
                           int displayOrder,
                           boolean display
                           ){
        this.id = id;
        this.name = name;
        this.type = type;
        this.displayOrder = displayOrder;
        this.display = display;
    }


    @Override
    public String getId() {
        return id;
    }

    @Override
    public SystemLovReference getReference() throws Exception {
        return ServiceLocator.getInstance().getReference(getId());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getDisplayOrder() {
        return displayOrder;
    }

    @Override
    public boolean display() {
        return display;
    }


}
