package com.squer.platform.core;

import com.squer.platform.core.reference.AppConfigReference;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.platform.services.entity.SquerEntity;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
@Entity(module = "core", since = "1.0", table = "FMK_APP_CONFIG", prefix = "APPCG")
public class AppConfig implements SquerEntity{

    @Attribute(column = "id", dbType="ID")
    private AppConfigReference reference;

    @Attribute(column = "app_version")
    private String appVersion;

    @Attribute(column = "admin_password")
    private String adminPassword;

    @Override
    public AppConfigReference getReference() {
        return reference;
    }

    @Override
    public void setReference(SquerReference reference) {
        this.reference = (AppConfigReference)reference;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String version) {
        this.appVersion = version;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }
}
