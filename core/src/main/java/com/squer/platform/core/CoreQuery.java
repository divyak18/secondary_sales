package com.squer.platform.core;

import com.squer.platform.persistence.FinderQuery;

/**
 * Created by ashutoshpavaskar on 06/12/16.
 */
public enum CoreQuery implements FinderQuery{
    LOV_REFERENCE_SELECT("lov_reference_select","name"),
    USER_SELECT("users_select","user_name"),
    USER_LOV_SELECT("usrlv_select","display_order"),
    USER_LOV_REF_SELECT("usrlv_reference_select","display_order"),
    SYSTEM_LOV_SELECT("syslv_select","display_order"),
    APPCG_SELECT("appcg_select"),
    APPCG_DELETE("appcg_delete"),
    USER_PRIV_SELECT("privs_for_user_select", "ci_name"),
    REFERENCE_PICKER("reference_picker");
    private String id;
    private String sortOrder;

    private CoreQuery(String id) {
        this.id = id;
    }

    private CoreQuery(String id, String sortOrder){
        this.id = id;
        this.sortOrder = sortOrder;
    }

    @Override
    public String sortField() {
        return sortOrder;
    }

    @Override
    public String id() {
        return id;
    }
}
