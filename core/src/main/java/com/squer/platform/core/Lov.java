package com.squer.platform.core;

import com.squer.platform.services.entity.SquerReference;

/**
 * Created by ashutoshpavaskar on 27/12/16.
 */
public interface Lov<T extends SquerReference>  {

    public T getReference();
    public void setReference(T reference);

    public String getName();
    public void setName(String value);

    public String getCiName();
    public void setCiName(String value);


    public int getDisplayOrder();
    public void setDisplayOrder(int order);

    public String getType();
    public void setType(String type);


}
