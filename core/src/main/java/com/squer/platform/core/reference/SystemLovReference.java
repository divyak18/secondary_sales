package com.squer.platform.core.reference;

import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;
import com.squer.platform.services.entity.impl.SquerReferenceImpl;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public class SystemLovReference extends SquerNamedReferenceImpl {


        public SystemLovReference(String id) {
            super(id, null);
        }

        public SystemLovReference() {
            super(null, null);
        }

        public SystemLovReference(String id, String name){
            super(id, name);
        }
}
