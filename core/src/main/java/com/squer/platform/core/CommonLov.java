package com.squer.platform.core;

import com.squer.platform.SystemLovConstants;
import com.squer.platform.core.reference.SystemLovReference;
import com.squer.platform.security.ServiceLocator;

/**
 * Created by mithila on 16/03/17.
 */
public enum CommonLov implements SystemLovConstants {
    ;



    private String id;
    private String name;
    private String type;
    private int displayOrder;
    private boolean display;

    private CommonLov(String id,
                           String name,
                           String type,
                           int displayOrder,
                           boolean display
    ){
        this.id = id;
        this.name = name;
        this.type = type;
        this.displayOrder = displayOrder;
        this.display = display;
    }

    @Override
    public String getId() {
        return id;
    }

    public SystemLovReference getReference() throws Exception{
        return ServiceLocator.getInstance().getReference(getId());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public int getDisplayOrder() {
        return displayOrder;
    }

    @Override
    public boolean display() {
        return display;
    }
}
