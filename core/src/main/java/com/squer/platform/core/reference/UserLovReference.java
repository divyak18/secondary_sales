package com.squer.platform.core.reference;

import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;
import com.squer.platform.services.entity.impl.SquerReferenceImpl;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public class UserLovReference extends SquerNamedReferenceImpl {

        public UserLovReference(String id) {
            super(id, null);
        }

        public UserLovReference() {
            super(null, null);
        }
        public UserLovReference(String id, String name){
            super(id, name);
        }
}
