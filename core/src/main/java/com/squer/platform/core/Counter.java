package com.squer.platform.core;

import com.squer.platform.core.reference.CounterReference;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;

/**
 * Created by ashutoshpavaskar on 17/03/17.
 */
@Entity(prefix = "COUNT", module = "core", table="FMK_COUNTER")
public class Counter implements SquerEntity{
    @Attribute(column = "id", dbType = "ID")
    private CounterReference reference;
    @Attribute(column = "count", dbType = "SERIAL")
    private long count;
    @Attribute(column = "owner_id", dbType = "M_STRING")
    private String owner;

    @Override
    public SquerReference getReference() {
        return reference;
    }

    @Override
    public void setReference(SquerReference reference) {
        this.reference = (CounterReference) reference;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
