package com.squer.platform.core;

import com.squer.platform.core.reference.SystemLovReference;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.impl.NamedImpl;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
@Entity(module = "core", since = "1.0", table = "FMK_SYSTEM_LOV", prefix = "SYSLV")
public class SystemLov extends NamedImpl implements  Lov{

    @Attribute(column = "lov_type", dbType="M_STRING")
    private String type;
    @Attribute(column = "display_order", dbType="INT")
    private int displayOrder;
    @Attribute(column = "display", dbType="BOOLEAN")
    private boolean display;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public boolean isDisplay() {
        return display;
    }

    public void setDisplay(boolean display) {
        this.display = display;
    }
}
