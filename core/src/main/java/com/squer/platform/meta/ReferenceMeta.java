package com.squer.platform.meta;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class ReferenceMeta {

    private String prefix;
    private String name;
    private String referenceClassName;
    private String entityClassName;
    private String tableName;
    private String model;
    private String module;

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReferenceClassName() {
        return referenceClassName;
    }

    public void setReferenceClassName(String referenceClassName) {
        this.referenceClassName = referenceClassName;
    }

    public String getEntityClassName() {
        return entityClassName;
    }

    public void setEntityClassName(String entityClassName) {
        this.entityClassName = entityClassName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
