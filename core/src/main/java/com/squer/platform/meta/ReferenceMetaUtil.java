package com.squer.platform.meta;

import com.squer.platform.CoreLogger;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemProperties;
import com.squer.platform.services.util.SystemProperty;
import com.squer.platform.services.util.SystemPropertyEnum;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class ReferenceMetaUtil {
    private static Map<String, ReferenceMeta> metaMap = new HashMap<>();
    private static Map<String, ReferenceMeta> metaMapByEntityClass = new HashMap<>();
    private static SquerLogger logger = SquerLogger.getLogger(CoreLogger.PERSISTENCE_LOGGER);

    public static ReferenceMeta getReferenceMeta(String inPrefix) throws Exception{
        String prefix = inPrefix.toLowerCase();
        if (metaMap.size() > 0)
            if (metaMap.containsKey(prefix))
                return metaMap.get(prefix);
            else
                throw new Exception("Prefix not found in meta:" + prefix);

        loadMeta();
        if (metaMap.containsKey(prefix))
            return metaMap.get(prefix);
        throw new Exception("Prefix not found in meta:" + prefix);
    }

    public static ReferenceMeta getReferenceMeta(Class entityClass) throws Exception{
        String className = entityClass.getName();
        if (metaMapByEntityClass.size() > 0)
            if (metaMapByEntityClass.containsKey(className))
                return metaMapByEntityClass.get(className);
        loadMeta();
        if (metaMapByEntityClass.containsKey(className))
            return metaMapByEntityClass.get(className);
        throw new Exception("Class not found in meta:" + className);
    }

    private static void loadMeta() throws Exception{
        ResourcePatternResolver patternResolver = new PathMatchingResourcePatternResolver();
        Resource [] resources = patternResolver.getResources("classpath*:reference/*-reference-meta.xml");
        logger.debug("No of resources:" + resources.length);
        for(Resource resource : resources)
        {
            logger.debug(resource.getFilename());
            SAXBuilder builder = new SAXBuilder();
            Document document = builder.build(resource.getInputStream());
            Element rootNode = document.getRootElement();
            List<Element> metaElements = rootNode.getChildren();
            for(Element metaElement : metaElements){
                ReferenceMeta meta = new ReferenceMeta();
                meta.setPrefix(metaElement.getChildText("prefix"));
                if(metaMap.containsKey(meta.getPrefix()))
                    throw new Exception("Prefix already declared:" + meta.getPrefix());
                meta.setTableName(metaElement.getChildText("table"));
                meta.setReferenceClassName(metaElement.getChild("reference").getAttributeValue("className"));
                meta.setEntityClassName(metaElement.getChildText("entity"));
                if(metaElement.getChild("model")!=null)
                    meta.setModel(metaElement.getChildText("model"));
                else
                    meta.setModel("BaseModel");
                meta.setModule(metaElement.getChildText("module"));
                logger.debug("loading the prefix in meta:" + meta.getPrefix());
                metaMap.put(meta.getPrefix(), meta);
                metaMapByEntityClass.put(meta.getEntityClassName(), meta);
            }
        }
    }
}
