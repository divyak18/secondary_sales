package com.squer.platform.persistence;

import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.exception.SquerException;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface Repository {

    public <T extends SquerReference>T create(SquerEntity entity) throws SquerException;

    public <T extends SquerReference>T update(SquerEntity  entity) throws SquerException;

    public void delete(SquerReference reference) throws SquerException;

    public <T extends SquerEntity>T restore(SquerReference reference) throws SquerException;

    public List find(CriteriaMap criteria) throws SquerException;

    public void fireAdhoc(CriteriaMap criteria) throws SquerException;


    public SquerReference getReference(String id) throws SquerException;
}
