package com.squer.platform.persistence.dao;

/**
 * Created by ashutoshpavaskar on 24/12/16.
 */
public class QueryCondition {
    private String column;
    private String sqlOperator = "=";
    private Object value;

    public QueryCondition(String column, String operator, Object value){
        this.column = column;
        this.value = value;
        this.sqlOperator = operator;
    }

    public QueryCondition(String column, Object value){
        this.column = column;
        this.value = value;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getSqlOperator() {
        return sqlOperator;
    }

    public void setSqlOperator(String sqlOperator) {
        this.sqlOperator = sqlOperator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
