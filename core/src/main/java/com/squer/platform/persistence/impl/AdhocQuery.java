package com.squer.platform.persistence.impl;

import com.squer.platform.persistence.FinderQuery;

/**
 * Created by ashutoshpavaskar on 07/12/16.
 */
public class AdhocQuery implements FinderQuery{



    private String id;

    private String sortOrder;

    public AdhocQuery(String id, String sortOrder){
        this.id = id;
        this.sortOrder = sortOrder;
    }

    public AdhocQuery(String id){
        this.id = id;
    }

    @Override
    public String id(){
        return id;
    }

    @Override
    public String sortField(){
        return sortOrder;
    }
}
