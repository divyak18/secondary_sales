package com.squer.platform.persistence;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public interface Delegates{
    public String id();

    public Delegates getDelegate(String id);
}
