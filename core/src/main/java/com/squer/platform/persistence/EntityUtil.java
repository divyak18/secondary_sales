package com.squer.platform.persistence;

import com.squer.platform.CoreLogger;
import com.squer.platform.core.SystemLov;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.entity.SquerNamedReference;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.meta.ReferenceMeta;
import com.squer.platform.meta.ReferenceMetaUtil;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.platform.services.exception.SquerException;
import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.StringUtil;
import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class EntityUtil {

    public static synchronized String generateGUID(String prefix) {
        return prefix + StringUtil.getUID();
    }

    private static Map<String, Delegates> delegatesMap = new HashMap<>();

    private static SquerLogger logger = SquerLogger.getLogger(CoreLogger.PERSISTENCE_LOGGER);


    public static <T extends SquerReference>T createReference(String id) throws SquerException{
        try {
            if(id==null || "".equalsIgnoreCase(id))
                return null;
            Class<T> cl = getReferenceClass(id);
            if(SquerNamedReference.class.isAssignableFrom(cl))
                return createReference(id, null);
            Constructor<T> cons = cl.getConstructor(String.class);
            return cons.newInstance(id);
        }catch (Exception e){
            throw new SquerException(e);
        }
    }


    public static <T extends  SquerNamedReference>T createReference(String id, String name) throws SquerException{
        try {
            if(id==null || "".equalsIgnoreCase(id))
                return null;
            Class<T> cl = getReferenceClass(id);
            Constructor<T> cons = cl.getConstructor(String.class, String.class);
            T reference = cons.newInstance(id, name);
            return  reference;
        }catch (Exception e){
            throw new SquerException(e);
        }
    }


    public static Delegates getDelegate(String name, String module) throws Exception {
        if (delegatesMap.containsKey(name)) {
            logger.debug("Found delegate in cache" + name + "=" + module);
            return delegatesMap.get(name);
        }

        Reflections reflections = new Reflections("com.squer." + module + ".registry");
        Set<Class<? extends  Delegates>> subTypes = reflections.getSubTypesOf(Delegates.class);
        if(subTypes==null || subTypes.isEmpty())
            throw new IllegalArgumentException("No delegates found for module " + module);
        for(Class clazz : subTypes) {
            logger.debug("Loading delegate:" + clazz.getClass().getName());
            Constructor<Delegates> constructor = clazz.getDeclaredConstructor(null);
            constructor.setAccessible(true);
            Delegates delegate = constructor.newInstance(null);
            Delegates instance = delegate.getDelegate(name);
            if (instance == null)
                throw new IllegalArgumentException("Invalid delegate:" + name);
            delegatesMap.put(name, instance);
            return instance;
        }
        throw new IllegalArgumentException("Invalid delegate:" + name);
    }


    private static Class getReferenceClass(String id) throws Exception {
        String prefix = id.substring(0, 5);
        ReferenceMeta meta = ReferenceMetaUtil.getReferenceMeta(prefix);
        String referenceClass =meta.getReferenceClassName();
        return Class.forName(referenceClass);
    }


}
