package com.squer.platform.persistence;

/**
 * Created by ashutoshpavaskar on 05/12/16.
 */
public interface FinderQuery {

    public String id();

    public String sortField();

}
