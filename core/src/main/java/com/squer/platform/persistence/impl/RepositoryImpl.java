package com.squer.platform.persistence.impl;

import com.squer.platform.meta.ReferenceMeta;
import com.squer.platform.meta.ReferenceMetaUtil;
import com.squer.platform.persistence.*;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.services.entity.Auditable;
import com.squer.platform.services.entity.NamedEntity;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.exception.SquerException;

import java.lang.reflect.Constructor;
import java.util.Date;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class RepositoryImpl implements Repository {

    public <T extends SquerReference>T create(SquerEntity entity) throws SquerException {
        try {
            Entity meta = entity.getClass().getAnnotation(Entity.class);
            if (entity.getReference() == null || entity.getReference().getId() == null) {
                String id = EntityUtil.generateGUID(meta.prefix());
                T reference = null;
                if(entity instanceof NamedEntity)
                    reference = EntityUtil.createReference(id, ((NamedEntity)entity).getName());
                else
                    reference = EntityUtil.createReference(id);
                entity.setReference(reference);
            }
            if (entity instanceof Auditable)
                fillAuditableForCreate((Auditable) entity);
            BaseModel model = getModel(meta);
            SquerEntity en = model.preCreate(meta, entity);
            en = model.create(meta, en);
            T reference = model.postCreate(meta, en);
            /*if (persistence.searchable().equalsIgnoreCase("true")) {
                SearchDao mongoDao = new SearchDao();
                mongoDao.insert(entity);
            }
            SquerEvent event = new SquerEvent(PlatformEvent.EntityCreated, ServiceLocator.getInstance().getPrincipal().getTenantId(), entity);
            ServiceLocator.getInstance().publishEvent(event);
            CacheManager.getInstance().put(PlatformCacheIdentifier.REFERENCE_DATA, reference.getId(), reference);
            */
            return reference;
        }catch (Exception e){
            throw new SquerException(e);
        }

    }

    public <T extends SquerReference>T update(SquerEntity entity) throws SquerException {
        try{
            Entity meta = entity.getClass().getAnnotation(Entity.class);
            if(entity instanceof Auditable)
                fillAuditableForUpdate((Auditable)entity);
            BaseModel model = getModel(meta);
            SquerEntity en = model.preUpdate(meta, entity);
            en = model.update(meta, en);
            T reference = model.postUpdate(meta, en);

            return reference;
        }catch (Exception e){
            e.printStackTrace();
            throw new SquerException(e);
        }
    }

    public void delete(SquerReference reference) throws SquerException {
        try{
            ReferenceMeta meta = ReferenceMetaUtil.getReferenceMeta(reference.getId().substring(0,5));
            Entity entityMeta = Class.forName(meta.getEntityClassName()).getAnnotation(Entity.class);
            BaseModel model = getModel(meta);
            model.preDelete(entityMeta, reference);
            model.delete(entityMeta, reference);
            model.postDelete(entityMeta, reference);
        }catch (Exception e){
            throw new SquerException(e);
        }
    }

    public <T extends SquerEntity>T restore(SquerReference reference) throws SquerException {
        try{
            ReferenceMeta meta = ReferenceMetaUtil.getReferenceMeta(reference.getId().substring(0,5));
            Entity entityMeta = Class.forName(meta.getEntityClassName()).getAnnotation(Entity.class);
            BaseModel model = getModel(meta);
            reference = model.preRestore(entityMeta, reference);
            T en = model.restore(entityMeta, reference);
            return model.postRestore(entityMeta, en);
        }catch (Exception e){
            throw new SquerException(e);
        }
    }


    public List find(CriteriaMap criteria) throws SquerException{
        BaseModel model = (BaseModel) ServiceLocator.getInstance().getBean(PlatformDelegates.BaseModel);
        List objects = null;
        try {
            objects = model.find(criteria);
        } catch (Exception e) {
            throw  new SquerException(e);
        }
        return objects;
    }


    public void fireAdhoc(CriteriaMap criteria) throws SquerException {
        BaseModel model =  (BaseModel)ServiceLocator.getInstance().getBean(PlatformDelegates.BaseModel);
        try {
            model.fireAdhoc(criteria);
        }catch (Exception e){
            throw new SquerException(e);
        }
    }


    @Override
    public SquerReference getReference(String id) throws SquerException{
        String prefix = id.substring(0,5).toLowerCase();
        AdhocQuery query = new AdhocQuery(prefix + "_reference_select","id");
        CriteriaMap criteria =new CriteriaMap(query);
        criteria.addCriteria("id", id);
        List<SquerReference> objects = find(criteria);
        return objects.get(0);
    }

    private BaseModel getModel(Entity meta) throws Exception {
        if(meta.model().equals("BaseModel"))
            return (BaseModel)ServiceLocator.getInstance().getBean(PlatformDelegates.BaseModel);
        return getModel(meta.module().toLowerCase(), meta.model());
    }

    private BaseModel getModel(ReferenceMeta meta) throws Exception{
        if(meta.getModel().equals("BaseModel"))
            return (BaseModel)ServiceLocator.getInstance().getBean(PlatformDelegates.BaseModel);
        return getModel(meta.getModule().toLowerCase(), meta.getModel());
    }


    private BaseModel getModel(String module, String model)throws Exception{
        /*Class clazz= Class.forName("com.squer.registry." + module + ".Delegates");
        Constructor<Delegates> constructor = clazz.getDeclaredConstructor(null);
        constructor.setAccessible(true);

        Delegates delegate = constructor.newInstance(null);
        Delegates bean = delegate.getDelegate(model);
        */

        Delegates bean = EntityUtil.getDelegate(model, module);
        BaseModel m = (BaseModel)ServiceLocator.getInstance().getBean(bean);
        return m;

    }

    private void fillAuditableForCreate(Auditable auditable){
        auditable.setCreatedBy(ServiceLocator.getInstance().getPrincipal().getId());
        auditable.setUpdatedBy(ServiceLocator.getInstance().getPrincipal().getId());
        Date now = new Date();
        auditable.setUpdatedOn(now);
        auditable.setCreatedOn(now);
        auditable.setStaleId(System.currentTimeMillis());
    }

    private void fillAuditableForUpdate(Auditable auditable){
        auditable.setUpdatedBy(ServiceLocator.getInstance().getPrincipal().getId());
        Date now = new Date();
        auditable.setUpdatedOn(now);
        auditable.setStaleId(System.currentTimeMillis());
    }

}
