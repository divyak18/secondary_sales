package com.squer.platform.persistence.dao;

import com.squer.platform.persistence.FinderQuery;
import com.squer.platform.services.util.SystemProperties;
import com.squer.platform.services.util.SystemPropertyEnum;

/**
 * Created by ashutoshpavaskar on 13/05/17.
 */
public class PagedCriteriaMap extends CriteriaMap{

    private static final int NUM_ROWS_PER_PAGE =
            Integer.parseInt(SystemProperties.getProperty(SystemPropertyEnum.SEARCH_NUM_ROWS));

    private int page;
    private int numRecords;
    private int numRows = -1;

    public PagedCriteriaMap(FinderQuery query) {
        super(query);
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
        super.addCriteria("limit","Y");
        super.addCriteria("numrows", NUM_ROWS_PER_PAGE);
        super.addCriteria("offset",getStartIndex());
    }

    public int getNumRecords() {
        return numRecords;
    }

    public void setNumRecords(int numRecords) {
        this.numRecords = numRecords;
    }

    public int getNumRows() {
        return numRows == -1? NUM_ROWS_PER_PAGE : numRows;
    }
    public int getStartIndex() {
        return (page-1) * getNumRows();
    }

}
