package com.squer.platform.persistence.dao;

import com.squer.platform.persistence.FinderQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 05/12/16.
 */
public class CriteriaMap {

    public static final String searchQuery = "searchQuery";

    private FinderQuery query = null;

    private Map<String, Object> criteria = new HashMap<>();

    private String orderBy;

    public CriteriaMap(FinderQuery query){
        this.query = query;
    }

    public FinderQuery getQuery(){
        return  this.query;
    }

    public void addCriteria(String key, Object value){
        criteria.put(key, value);
    }



    public Map<String, Object> getCriteria(){
        return criteria;
    }

    public void addSearchConditions(List<QueryCondition> conditions){
        criteria.put("searchQuery", conditions);
    }


    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }
}
