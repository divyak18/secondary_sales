package com.squer.platform.persistence;

import com.squer.platform.core.CoreQuery;
import com.squer.platform.core.Lov;
import com.squer.platform.core.SystemLov;
import com.squer.platform.core.UserLov;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class ReferenceUtil {


    private static HashMap<String, SquerNamedReferenceImpl> lovCache = new HashMap<>();

    public static SquerNamedReferenceImpl getLov(String id) throws Exception {
        if(lovCache.containsKey(id))
            return  lovCache.get(id);

        Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
        CriteriaMap criteriaMap = new CriteriaMap(CoreQuery.LOV_REFERENCE_SELECT);
        List<SquerNamedReferenceImpl> lovList = repository.find(criteriaMap);
        for(SquerNamedReferenceImpl lov : lovList) {
            lovCache.put(lov.getId(), lov);
        }

        if(lovCache.containsKey(id))
            return  lovCache.get(id);
        throw  new IllegalArgumentException("Invalid LOV:" + id);
    }



}
