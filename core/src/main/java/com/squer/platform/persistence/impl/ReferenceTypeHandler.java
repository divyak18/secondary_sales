package com.squer.platform.persistence.impl;

import com.squer.platform.core.CoreQuery;
import com.squer.platform.core.Lov;
import com.squer.platform.core.reference.SystemLovReference;
import com.squer.platform.core.reference.UserLovReference;
import com.squer.platform.meta.ReferenceMeta;
import com.squer.platform.meta.ReferenceMetaUtil;
import com.squer.platform.persistence.EntityUtil;
import com.squer.platform.persistence.ReferenceUtil;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.entity.SquerNamedReference;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ashutoshpavaskar
 * Date: 17/09/13
 * Time: 5:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReferenceTypeHandler implements TypeHandler {
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, Object o, JdbcType jdbcType) throws SQLException {

    }

    @Override
    public Object getResult(ResultSet resultSet, String s) throws SQLException {
        try{

            String ownerId = resultSet.getString(s);
            if(ownerId==null || "".equals(ownerId))
                return null;
            if (ownerId.startsWith("SYSLV") || ownerId.startsWith("USRLV"))
            {
                SquerNamedReferenceImpl namedReference = ReferenceUtil.getLov(ownerId);
                if (ownerId.startsWith("SYSLV"))
                    return new SystemLovReference(namedReference.getId(), namedReference.getName());
                else
                    return new UserLovReference(namedReference.getId(), namedReference.getName());
            }else{
                SquerReference reference= ServiceLocator.getInstance().getReference(ownerId);
                if(reference instanceof SquerNamedReference){
                    Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                    ReferenceMeta meta = ReferenceMetaUtil.getReferenceMeta(reference.getId().substring(0,5));
                    CriteriaMap criteria = new CriteriaMap(CoreQuery.REFERENCE_PICKER);
                    criteria.addCriteria("tableName", meta.getTableName());
                    criteria.addCriteria("id", reference.getId());
                    List rs = repository.find(criteria);
                    if (rs.isEmpty() || rs == null)
                        return (SquerNamedReferenceImpl) reference;

                    SquerNamedReferenceImpl ni = (SquerNamedReferenceImpl) rs.get(0);
                    ((SquerNamedReferenceImpl) reference).setName (ni.getName());

                    return (SquerNamedReferenceImpl)reference;
                }
            }
            return EntityUtil.createReference(ownerId);
        }catch (Exception e){
            throw new SQLException(e);
        }
    }

    @Override
    public Object getResult(ResultSet resultSet, int i) throws SQLException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getResult(CallableStatement callableStatement, int i) throws SQLException {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
