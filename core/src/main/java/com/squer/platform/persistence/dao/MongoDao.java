package com.squer.platform.persistence.dao;


import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.squer.core.registry.CoreSystemProperties;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.services.exception.SquerException;
import com.squer.platform.services.util.SystemProperties;
import org.apache.commons.collections.IteratorUtils;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import java.net.UnknownHostException;
import java.util.List;

public class MongoDao {

    public static final String MONGODB_NAME = SystemProperties.getProperty(CoreSystemProperties.MongoDb);

    public List<Object> find(String collectionName, Class className,String queryString) throws SquerException, UnknownHostException {
        MongoCollection collection = getCollection(collectionName);
        return IteratorUtils.toList(collection.find(queryString).as(className));
    }

    public boolean delete(String collectionName,String queryString) throws SquerException,UnknownHostException{
        try {
            MongoCollection collection = getCollection(collectionName);
            collection.remove(queryString);
            return true;
        }catch (Exception e){
            throw  new SquerException(e);
        }
    }

    public boolean insert(String collectionName,Object entity)throws SquerException,UnknownHostException{
        try {
            MongoCollection collection=getCollection(collectionName);
            collection.save(entity);
            return true;
        }catch (Exception e){
            throw  new SquerException(e);
        }
    }

    private static MongoCollection getCollection(String collectionName) throws SquerException, UnknownHostException {
        MongoClient client = MongoDatasource.getMongoClient();
        DB db = client.getDB(MONGODB_NAME);
        Jongo jongo = new Jongo(db);
        return jongo.getCollection(collectionName);
    }
}
