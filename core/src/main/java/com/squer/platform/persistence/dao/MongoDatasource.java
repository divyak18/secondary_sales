package com.squer.platform.persistence.dao;

import com.mongodb.MongoClient;
import com.squer.core.registry.CoreSystemProperties;
import com.squer.platform.services.exception.SquerException;
import com.squer.platform.services.util.SystemProperties;

import java.net.UnknownHostException;

public class MongoDatasource {
    private static MongoClient client=null;
    public static MongoClient getMongoClient() throws SquerException, UnknownHostException {
        String host= SystemProperties.getProperty(CoreSystemProperties.MongoHost);
        int port= Integer.parseInt(SystemProperties.getProperty((CoreSystemProperties.MongoPort)));
        if(client==null)
            client=new MongoClient(host,port);

        return client;
    }
}
