package com.squer.platform.persistence;

import com.squer.platform.core.Counter;
import com.squer.platform.services.entity.SquerReference;

/**
 * Created by ashutoshpavaskar on 17/03/17.
 */
public class CounterUtil {

    public static long getNextCount(Repository repository, SquerReference reference) throws Exception {
        Counter counter = new Counter();
        counter.setOwner(reference.getId());
        SquerReference counterReference = repository.create(counter);
        counter = repository.restore(counterReference);
        return counter.getCount();
    }
}
