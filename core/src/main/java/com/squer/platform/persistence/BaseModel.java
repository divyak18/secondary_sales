package com.squer.platform.persistence;

import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Entity;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public interface BaseModel {

    public <T extends SquerEntity>T preCreate(Entity meta, T entity) throws Exception;
    public <T extends SquerEntity>T create(Entity meta, T entity) throws Exception;
    public <T extends SquerReference>T postCreate(Entity meta, SquerEntity entity) throws Exception;

    public <T extends SquerEntity>T preUpdate(Entity meta, T entity) throws Exception;
    public <T extends SquerEntity>T update(Entity meta, T entity) throws Exception;
    public <T extends SquerReference>T postUpdate(Entity meta, SquerEntity entity) throws Exception;

    public <T extends SquerReference>T preRestore(Entity meta, T reference) throws Exception;
    public <T extends SquerEntity>T restore(Entity meta, SquerReference reference) throws Exception;
    public <T extends SquerEntity>T postRestore(Entity meta, T entity) throws Exception;

    public void preDelete(Entity meta, SquerReference reference) throws Exception;
    public void delete(Entity meta, SquerReference reference) throws Exception;
    public void postDelete(Entity meta, SquerReference reference) throws Exception;


    public List find(CriteriaMap criteria) throws Exception;

    public void fireAdhoc(CriteriaMap criteriaMap) throws Exception;
}
