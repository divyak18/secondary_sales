package com.squer.platform.persistence.impl;

import com.squer.platform.persistence.BaseModel;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.persistence.dao.SqlDao;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Entity;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 27/11/16.
 */
public class BaseModelImpl implements BaseModel {

    private SqlDao sqlDao;


    public <T extends SquerEntity>T preCreate(Entity meta, T entity) throws Exception {
        return entity;
    }

    @Override
    public <T extends SquerEntity>T create(Entity meta, T entity) throws Exception {

        getSqlDao().insert(meta.prefix().toLowerCase() + "_insert", entity);
        return entity;
    }

    @Override
    public <T extends  SquerReference>T postCreate(Entity meta, SquerEntity entity) throws Exception {
        return (T)entity.getReference();
    }

    @Override
    public <T extends SquerEntity>T preUpdate(Entity meta, T entity) throws Exception{
        return entity;
    }

    @Override
    public <T extends SquerEntity>T update(Entity meta, T entity) throws Exception{
        getSqlDao().update(meta.prefix().toLowerCase() + "_update", entity);
        return entity;
    }

    @Override
    public <T extends SquerReference>T postUpdate(Entity meta, SquerEntity entity) throws Exception {
        return (T)entity.getReference();
    }




    public <T extends SquerReference>T preRestore(Entity meta, T reference) throws Exception{
        return reference;
    }

    public <T extends SquerEntity>T restore(Entity meta, SquerReference reference) throws Exception{
        CriteriaMap criteria = new CriteriaMap(new AdhocQuery(meta.prefix().toLowerCase() + "_select"));
        criteria.addCriteria("id", reference.getId());
        List<T> list =  getSqlDao().select(criteria);
        if(list.isEmpty() || list.size()==0)
            throw new Exception("Invalid id:" + reference.getId());
        return list.get(0);
    }

    @Override
    public <T extends SquerEntity>T postRestore(Entity meta, T entity) throws Exception{
        return entity;
    }


    public void preDelete(Entity meta, SquerReference reference) throws Exception{
        return;
    }



    public void delete(Entity meta, SquerReference reference) throws Exception {
        CriteriaMap criteria = new CriteriaMap(new AdhocQuery(meta.prefix().toLowerCase() + "_delete"));
        criteria.addCriteria("id",reference.getId());
        getSqlDao().delete(criteria);
    }

    public void postDelete(Entity meta,  SquerReference reference) throws Exception {
        return;
    }

    public List find(CriteriaMap criteria) throws Exception{
        return getSqlDao().select(criteria);
    }

    public void fireAdhoc(CriteriaMap criteriaMap) throws Exception {
        String query = criteriaMap.getQuery().id();
        if(query.endsWith("insert"))
            getSqlDao().insert(query,criteriaMap.getCriteria());
        else if(query.endsWith("delete"))
            getSqlDao().delete(criteriaMap);
        else if(query.endsWith("update"))
            getSqlDao().update(query,criteriaMap.getCriteria());
    }


    public SqlDao getSqlDao() {
        return sqlDao;
    }

    public void setSqlDao(SqlDao sqlDao) {
        this.sqlDao = sqlDao;
    }



}
