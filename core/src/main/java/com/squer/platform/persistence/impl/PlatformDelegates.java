package com.squer.platform.persistence.impl;

import com.squer.platform.persistence.Delegates;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 30/11/16.
 */
public class PlatformDelegates implements Delegates{

    private static Map<String, Delegates> delegatesMap = new HashMap<>();

    public static final Delegates BaseModel = new PlatformDelegates("BaseModel");

    public static final Delegates Repository = new PlatformDelegates("Repository");

    private String id;

    private PlatformDelegates(String id){
        this.id = id;
        delegatesMap.put(id, this);
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public Delegates getDelegate(String id) {
        return delegatesMap.get(id);
    }
}
