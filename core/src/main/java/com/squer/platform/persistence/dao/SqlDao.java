package com.squer.platform.persistence.dao;

import com.squer.platform.CoreLogger;
import com.squer.platform.services.exception.SquerException;
import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.SystemPropertyEnum;
import com.sun.javafx.runtime.SystemProperties;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 22/11/16.
 */
public class SqlDao {
    private static SquerLogger logger = SquerLogger.getLogger(CoreLogger.PERSISTENCE_LOGGER);
    private SqlSession sqlSession;

    public Object insert(String query, Object entity) throws SquerException {
        logger.debug(query);
        return getSqlSession().insert(query, entity);
    }

    public Object update(String query, Object entity) throws SquerException {
        logger.debug(query);
        return getSqlSession().update(query, entity);
    }

    public void delete(CriteriaMap criteria) throws SquerException{
        getSqlSession().delete(criteria.getQuery().id(), criteria.getCriteria());
    }

    public List select(CriteriaMap criteria) throws SquerException{

        if (criteria.getOrderBy() == null || "".equalsIgnoreCase(criteria.getOrderBy().trim())){
            criteria.setOrderBy(criteria.getQuery().sortField() + " asc");
        }

        if(!(criteria instanceof PagedCriteriaMap))
            return getSqlSession().selectList(criteria.getQuery().id(), criteria.getCriteria());

        PagedCriteriaMap pageable = (PagedCriteriaMap)criteria;
        int count = getSqlSession().selectOne(criteria.getQuery().id() + "_count",criteria.getCriteria());

        criteria.addCriteria("offset",pageable.getStartIndex());
        criteria.addCriteria("numrows",pageable.getNumRows());
        List list = getSqlSession().selectList(criteria.getQuery().id(), criteria.getCriteria());
        return new PageableList(list, count);


    }

    public SqlSession getSqlSession() {
        return sqlSession;
    }

    public void setSqlSession(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }
}

