package com.squer.platform.persistence.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 13/05/17.
 */
public class PageableList<E>  extends ArrayList<E> {

    private int totalRecords;
    private List result;

    public PageableList(){

    }

    public PageableList(List list, int totalRecords){
        addAll(list);
        this.result = list;
        this.totalRecords = totalRecords;
    }

    public void setTotalRecords(int i){
        //DO nothing
    }

    public int getTotalRecords(){
        return totalRecords;
    }

    public List getResult() {
        return result;
    }

    public void setResult(List result) {
        this.result = result;
    }


}
