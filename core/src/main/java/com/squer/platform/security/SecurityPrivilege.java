package com.squer.platform.security;

import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;

/**
 * Created by ashutoshpavaskar on 02/05/17.
 */
@Entity(prefix="PRIVG", table = "FMK_SECURITY_PRIVILEGE", module = "core")
public class SecurityPrivilege implements SquerEntity {

    @Attribute(column = "ID", dbType = "ID")
    private SquerReference reference;

    @Attribute(column = "PRIV_NAME", dbType = "L_STRING")
    private String name;

    @Attribute(column = "PRIV_DESCRIPTION", dbType = "TEXT")
    private String description;


    @Override
    public SquerReference getReference() {
        return reference;
    }

    @Override
    public void setReference(SquerReference reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
