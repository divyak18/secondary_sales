package com.squer.platform.security;

import com.squer.platform.persistence.FinderQuery;

/**
 * Created by ashutoshpavaskar on 06/12/16.
 */
public enum SecurityQuery implements FinderQuery{
    USERS_SELECT("users_select", "user_name"),
    USERS_DELETE("users_delete");

    private String id;
    private String sortOrder;

    private SecurityQuery(String id){
        this.id = id;
    }
    private SecurityQuery(String id, String sortOrder){
        this.id = id;
        this.sortOrder = sortOrder;
    }

    @Override
    public String sortField(){
        return sortOrder;
    }

    @Override
    public String id() {
        return id;
    }
}
