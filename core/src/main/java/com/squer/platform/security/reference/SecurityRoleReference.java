package com.squer.platform.security.reference;

import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;
import com.squer.platform.services.entity.impl.SquerReferenceImpl;

/**
 * Created by ashutoshpavaskar on 07/12/16.
 */
public class SecurityRoleReference extends SquerNamedReferenceImpl {

    public SecurityRoleReference() {
        super(null, null);
    }

    public SecurityRoleReference(String id, String name){
        super(id, name);
    }
}
