package com.squer.platform.security;

import java.security.Principal;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public class SquerPrincipal implements Principal {
    private String id;
    private String name;
    private UserEntity user;
    private Map<String, Object> additionalParam = new HashMap<>();

    public SquerPrincipal(String id, UserEntity user, String name) {
        this.id = id;
        this.name = name;
        this.user = user;
    }

    public String getId(){
        return id;
    }


    public String getName(){
        return name;
    }

    public UserEntity getUser() {
        return user;
    }

    public void addParam(String parameter, Object value){
        additionalParam.put(parameter, value);
    }

    public void addParams(Map<String, Object> additionalParam ){
        this.additionalParam.putAll(additionalParam);
    }

    public Object getAdditionalParam(String parameter){
        return additionalParam.get(parameter);
    }
}