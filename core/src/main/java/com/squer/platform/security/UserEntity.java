package com.squer.platform.security;

import com.squer.platform.core.reference.SystemLovReference;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.impl.AuditableImpl;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.platform.security.reference.UserEntityReference;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */

@Entity(prefix="USERS", table = "FMK_SECURITY_USER", module = "core", model="UserEntityModel")
public class UserEntity extends AuditableImpl {

    public final static SystemLovReference ACTIVE = new SystemLovReference("SYSLV00000000000000000000000000000006");
    public final static SystemLovReference INACTIVE = new SystemLovReference("SYSLV00000000000000000000000000000007");
    public static final String ADMIN_USER_ID = "USERS00000000000000000000000000000001";
    public static final String GUEST_USER_ID = "USERS00000000000000000000000000000000";
    public static final String SCHEDULER_ADMIN_USER_ID = "USERS00000000000000000000000000000002";


    @Attribute(column = "status_id", dbType = "ID")
    private SystemLovReference status;
    @Attribute(column = "user_name", dbType = "M_STRING")
    private String userName;
    @Attribute(column = "password", dbType = "L_STRING")
    private String password;
    @Attribute(column = "owner_id", dbType = "ID", mapped = "owner.id")
    private SquerReference owner;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SquerReference getOwner() {
        return owner;
    }

    public void setOwner(SquerReference owner) {
        this.owner = owner;
    }

    public SystemLovReference getStatus() {
        return status;
    }

    public void setStatus(SystemLovReference status) {
        this.status = status;
    }
}
