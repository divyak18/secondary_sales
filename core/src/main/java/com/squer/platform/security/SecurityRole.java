package com.squer.platform.security;

import com.squer.platform.security.reference.SecurityPrivilegeReference;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.entity.impl.StandardEntity;
import com.squer.platform.services.entity.meta.Entity;

import java.util.List;

/**
 * Created by ashutoshpavaskar on 02/05/17.
 */
@Entity(prefix="SROLE", table = "FMK_SECURITY_ROLE", module = "core")
public class SecurityRole extends StandardEntity {

    private List<SecurityPrivilegeReference> privileges;

    public List<SecurityPrivilegeReference> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<SecurityPrivilegeReference> privileges) {
        this.privileges = privileges;
    }
}
