package com.squer.platform.security.reference;

import com.squer.platform.services.entity.impl.SquerReferenceImpl;

/**
 * Created by ashutoshpavaskar on 07/12/16.
 */
public class UserEntityReference extends SquerReferenceImpl {

    public UserEntityReference(String id){
        super(id);
    }
}
