package com.squer.platform.security.model;

import com.squer.platform.persistence.impl.BaseModelImpl;
import com.squer.platform.security.ServiceLocator;
import com.squer.platform.security.UserEntity;
import com.squer.platform.services.entity.SquerEntity;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.platform.services.util.StringUtil;
import com.squer.platform.services.util.SystemProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 17/05/17.
 */
public class UserEntityModel extends BaseModelImpl {

    public <T extends SquerEntity> T preCreate(Entity meta, T entity) throws Exception {
        UserEntity user = (UserEntity) entity;
        user.setStatus(UserEntity.ACTIVE);
        if (user.getPassword()== null || user.getPassword().trim().equalsIgnoreCase("")) {
            String password = "welcome";
            user.setPassword(StringUtil.getHash(password));
        }
        return (T)user;
    }

}