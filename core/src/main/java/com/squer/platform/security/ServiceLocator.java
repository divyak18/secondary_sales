package com.squer.platform.security;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */

import com.squer.platform.CoreLogger;
import com.squer.platform.persistence.Delegates;
import com.squer.platform.persistence.EntityUtil;
import com.squer.platform.services.entity.SquerReference;
import com.squer.platform.services.log.SquerLogger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


public class ServiceLocator {
    private static final String[] springfiles = new String[]{"spring-all.xml"};
    private static  ServiceLocator locator = null;
    private static ApplicationContext context = null;


    SquerLogger logger =  SquerLogger.getLogger(CoreLogger.PERSISTENCE_LOGGER);

    private ServiceLocator(){
        try {
            if (locator != null)
                throw new RuntimeException("ServiceLocator instance created twice.");
            if (context == null)
                context = new ClassPathXmlApplicationContext(springfiles);
        }catch (Exception e){
            logger.error(e);
            e.printStackTrace();
        }
    }

    public static ServiceLocator getInstance(){
        if(locator==null)
            locator=new ServiceLocator();
        return locator;
    }

    public Object getBean(Delegates delegate){
        return context.getBean(delegate.id());
    }

    public SquerPrincipal getPrincipal(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth!=null && auth instanceof AnonymousAuthenticationToken)
            return null;
        if(auth!=null){
            return (SquerPrincipal)auth.getPrincipal();
        }
        return null;
    }

    public Resource[] loadResource(String path) throws Exception{
        return context.getResources(path);
    }


    public <T extends SquerReference>T getReference(String id) throws Exception{
        return EntityUtil.createReference(id);
    }


    public <T extends SquerReference>T getReference(String id, String name) throws Exception{
        return EntityUtil.createReference(id, name);
    }

    /*
    public void publishEvent(SquerEvent event) throws SquerException {
        try{
            SquerEventPublisher publisher = (SquerEventPublisher)getBean(PlatformDelegates.SquerEventPublisher);
            publisher.setSquerEvent(event);
            publisher.publish();
        }catch (Exception e){
            throw new SquerException(e);
        }
    }
    */
    /*  public Class getEntityClass(String prefix) throws Exception{
        return ReferenceLoader.getInstance().getEntityClass(prefix);
    }



    public NamedReference getReference(String id, String name) throws Exception{
        return ReferenceLoader.getInstance().getReference(id, name);
    }
    */
}
