package com.squer.platform.security;

import com.squer.platform.CoreLogger;
import com.squer.platform.services.exception.SquerException;
import com.squer.platform.services.log.SquerLogger;
import com.squer.platform.services.util.StringUtil;
import java.util.StringTokenizer;

/**
 * Created by ashutoshpavaskar on 07/12/16.
 */
public class AuthCertificate {
    private static SquerLogger logger = SquerLogger.getLogger(CoreLogger.SECURITY_LOGGER);
    private String id;
    private long expiresOn;
    private String name;
    private String ownerId;

    public AuthCertificate(SquerPrincipal principal, long expiresOn) throws SquerException {
        try{
            this.id = principal.getId();
            this.expiresOn = expiresOn;
            this.name = principal.getName();
            if(principal.getUser() != null && principal.getUser().getOwner() != null)
                this.ownerId = principal.getUser().getOwner().getId();

            /*if(this.expiresOn<System.currentTimeMillis())
                throw new SquerException(PlatformExceptionMessage.ExpiredCert);*/
        }catch (Exception e){
            logger.error(e);
            throw new SquerException(e);
        }
    }
    public AuthCertificate(String certificate) throws SquerException {
        try{
            String decrypted = StringUtil.decrypt(certificate);
            StringTokenizer tokenizer = new StringTokenizer(decrypted,":");
            this.id = tokenizer.nextToken();
            this.name = tokenizer.nextToken();
            this.expiresOn = Long.parseLong(tokenizer.nextToken());
            if(tokenizer.hasMoreTokens())
                this.ownerId = tokenizer.nextToken();

            /*if(this.expiresOn<System.currentTimeMillis())
                throw new SquerException(PlatformExceptionMessage.ExpiredCert);*/
        }catch (Exception e){
            logger.error(e);
            throw new SquerException(e);
        }
    }

    public String encrypt() throws Exception{
        String value = id + ':' + name + ":" +  expiresOn;

        if(!StringUtil.isNullOrEmpty(ownerId)){
            value += ':'+ownerId;
        }

        return StringUtil.encrypt(value);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getExpiresOn() {
        return expiresOn;
    }

    public void setExpiresOn(long expiresOn) {
        this.expiresOn = expiresOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
