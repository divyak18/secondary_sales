package com.squer.platform.security.reference;

import com.squer.platform.services.entity.impl.SquerReferenceImpl;

/**
 * Created by ashutoshpavaskar on 07/12/16.
 */
public class SecurityPrivilegeReference extends SquerReferenceImpl {

    public SecurityPrivilegeReference(String id){
        super(id);
    }
}
