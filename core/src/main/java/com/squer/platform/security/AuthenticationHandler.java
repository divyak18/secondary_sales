package com.squer.platform.security;

import com.squer.platform.CoreSystemLovs;
import com.squer.platform.core.AppConfig;
import com.squer.platform.core.CoreQuery;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.dao.CriteriaMap;
import com.squer.platform.persistence.dao.QueryCondition;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.security.reference.UserEntityReference;
import com.squer.platform.services.exception.CoreExceptionEnum;
import com.squer.platform.services.exception.SquerException;
import com.squer.platform.services.util.StringUtil;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashutoshpavaskar on 07/12/16.
 */
public class AuthenticationHandler {

    public static final long CERT_VALIDITY = 24*60*60*1000*10;
    public static final long GUEST_VALIDITY = Long.MAX_VALUE;

    public static AuthCertificate authenticateAsAdmin() throws SquerException{
        try {
            SquerPrincipal principal = new SquerPrincipal(UserEntity.ADMIN_USER_ID, new UserEntity(), "Admin");
            return getAuthCertificate(principal);
        }catch (SquerException e){
            throw e;
        }catch (Exception e){
            throw new SquerException(e);
        }
    }

    public static AuthCertificate authenticateAsSchedulerAdmin() throws SquerException {
        try {
            UserEntity user= new UserEntity();
            UserEntityReference reference = ServiceLocator.getInstance().getReference(UserEntity.SCHEDULER_ADMIN_USER_ID);
            user.setReference(reference);

            SquerPrincipal principal = new SquerPrincipal(UserEntity.SCHEDULER_ADMIN_USER_ID, user, "SchedulerAdmin");
            return getAuthCertificate(principal);
        }catch (SquerException e){
            throw e;
        }catch (Exception e){
            throw new SquerException(e);
        }
    }

    private static AuthCertificate getAuthCertificate(SquerPrincipal principal) throws Exception {
        AuthCertificate certificate = new AuthCertificate(principal, GUEST_VALIDITY);
        setContext(principal, certificate);
        return certificate;
    }


    public static AuthCertificate authenticateAsGuest() throws SquerException {
        try {
            SquerPrincipal principal = new SquerPrincipal(UserEntity.GUEST_USER_ID, new UserEntity(), "Guest");
            return getAuthCertificate(principal);
        }catch (SquerException e){
            throw e;
        }catch (Exception e){
            throw new SquerException(e);
        }
    }

    public static AuthCertificate authenticate(String userName, String password) throws SquerException {
        try{
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            if("admin".equalsIgnoreCase(userName)){
                CriteriaMap criteria = new CriteriaMap(CoreQuery.APPCG_SELECT);
                List<AppConfig> configs = repository.find(criteria);
                if(configs==null || configs.isEmpty() || configs.size()!=1)
                    throw new IllegalStateException("Invalid configuration");
                AppConfig config = configs.get(0);
                if(!config.getAdminPassword().equals(StringUtil.getHash(password)))
                    throw new SquerException(CoreExceptionEnum.INVALID_CREDENTIALS);
                UserEntity user= new UserEntity();
                UserEntityReference reference = ServiceLocator.getInstance().getReference(UserEntity.ADMIN_USER_ID);
                user.setReference(reference);

                SquerPrincipal principal = new SquerPrincipal(UserEntity.ADMIN_USER_ID,user,"Admin");
                AuthCertificate cert = new AuthCertificate(principal, System.currentTimeMillis() + CERT_VALIDITY);
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority("USER"));
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal,cert,grantedAuths);
                SecurityContextHolder.getContext().setAuthentication(token);
                return cert;
            }else{
                CriteriaMap criteria = new CriteriaMap(SecurityQuery.USERS_SELECT);
                List<QueryCondition> conditions = new ArrayList<>();
                QueryCondition condition = new QueryCondition("user_name", userName);
                conditions.add(condition);
                criteria.addCriteria("searchQuery", conditions);
                List<UserEntity> users = repository.find(criteria);
                if(users==null || users.isEmpty())
                    throw new AuthenticationCredentialsNotFoundException("Invalid user");
                UserEntity user= users.get(0);
                user = repository.restore(user.getReference());
                if(!user.getStatus().getId().equals(CoreSystemLovs.ACTIVE_STATUS.getId())
                        || !user.getPassword().equals(StringUtil.getHash(password)))
                    throw new AuthenticationCredentialsNotFoundException("Invalid user");

                SquerPrincipal principal = new SquerPrincipal(user.getReference().getId(),user, user.getUserName());
                AuthCertificate cert = new AuthCertificate(principal, System.currentTimeMillis() + CERT_VALIDITY);
                List<GrantedAuthority> grantedAuths = new ArrayList<>();
                grantedAuths.add(new SimpleGrantedAuthority("USER"));
                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal,cert,grantedAuths);
                SecurityContextHolder.getContext().setAuthentication(token);
                return cert;
            }
        }catch(SquerException e) {
            e.printStackTrace();
            throw e;
        }catch(AuthenticationCredentialsNotFoundException e){
            throw new SquerException(CoreExceptionEnum.INVALID_CREDENTIALS);
        }catch (Exception e){
            throw new SquerException(e);
        }
    }

    public static AuthCertificate authenticate(SquerPrincipal principal) throws SquerException{
        try {
            AuthCertificate certificate = new AuthCertificate(principal, System.currentTimeMillis() + CERT_VALIDITY);
            List<GrantedAuthority> grantedAuths = new ArrayList<>();
            grantedAuths.add(new SimpleGrantedAuthority("USER"));
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal, certificate, grantedAuths);
            SecurityContextHolder.getContext().setAuthentication(token);
            return certificate;
        }catch(Exception e){
            throw new SquerException(e);
        }
    }

    public static AuthCertificate authenticate(String certString) throws SquerException{

        try {
            AuthCertificate certificate = new AuthCertificate(certString);

            UserEntity user = new UserEntity();
            user.setReference(ServiceLocator.getInstance().getReference(certificate.getId()));
            if(!StringUtil.isNullOrEmpty(certificate.getOwnerId())) {
                user.setOwner(ServiceLocator.getInstance().getReference(certificate.getOwnerId()));
            }else{
                if(!certificate.getId().equals(UserEntity.ADMIN_USER_ID)) {
                    Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
                    user = (UserEntity) repository.restore(ServiceLocator.getInstance().getReference(certificate.getId()));
                    certificate.setOwnerId(user.getOwner().getId());
                }
            }

            SquerPrincipal principal = new SquerPrincipal(certificate.getId(),
                    user,
                    certificate.getName());
            setContext(principal, certificate);
            return certificate;
        }catch(Exception e){
            throw new SquerException(e);
        }
    }

    private static void setContext(SquerPrincipal principal, AuthCertificate certificate) throws Exception{
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(principal,certificate);
        SecurityContextHolder.getContext().setAuthentication(token);
        //SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);

        if(!principal.getId().equalsIgnoreCase(UserEntity.ADMIN_USER_ID) && !principal.getId().equalsIgnoreCase(UserEntity.SCHEDULER_ADMIN_USER_ID)
                && !principal.getId().equalsIgnoreCase(UserEntity.GUEST_USER_ID)) {
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            CriteriaMap criteria = new CriteriaMap(SecurityQuery.USERS_SELECT);
            criteria.addCriteria("id", principal.getId());
            List<UserEntity> USERS = repository.find(criteria);
            if (USERS==null || USERS.isEmpty())
                throw new SquerException(CoreExceptionEnum.INVALID_CREDENTIALS);
            UserEntity user = USERS.get(0);
            if (!user.getStatus().getId().equalsIgnoreCase(CoreSystemLovs.ACTIVE_STATUS.getId()))
                throw new SquerException(CoreExceptionEnum.INVALID_CREDENTIALS);
        }
    }

}



