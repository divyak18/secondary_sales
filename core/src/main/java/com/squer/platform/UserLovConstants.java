package com.squer.platform;

import com.squer.platform.core.reference.UserLovReference;

public interface UserLovConstants {

    public String getId();

    public UserLovReference getReference() throws Exception;

    public String getName();

    public String getType();

    public int getDisplayOrder();

    public boolean display();
}
