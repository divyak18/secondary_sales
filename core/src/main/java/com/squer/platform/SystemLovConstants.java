package com.squer.platform;

import com.squer.platform.core.reference.SystemLovReference;

/**
 * Created by ashutoshpavaskar on 28/12/16.
 */
public interface SystemLovConstants {
    public String getId();

    public SystemLovReference getReference() throws Exception;

    public String getName();

    public String getType();

    public int getDisplayOrder();

    public boolean display();

}
