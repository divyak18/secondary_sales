package com.squer.platform;

import com.squer.platform.services.log.LoggerName;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public enum CoreLogger implements LoggerName {

    PERSISTENCE_LOGGER("persistence"),
    SECURITY_LOGGER("security");

    private String name;
    private CoreLogger(String name){
        this.name = name;
    }

    @Override
    public String logger() {
        return name;
    }
}
