package com.squer.test.platform.persistence;

import com.squer.platform.persistence.Delegates;
import com.squer.platform.persistence.EntityUtil;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by ashutoshpavaskar on 17/05/17.
 */
public class EntityUtilTest {

    @Test(groups="Persistence")
    public void getDelegates() {
        try {
            Delegates bean  = EntityUtil.getDelegate("EmployeeModel","common");
            Assert.assertNotNull(bean);
            bean  = EntityUtil.getDelegate("EmployeeModel","common");
            Assert.assertNotNull(bean);
        }catch (Exception e){
            Assert.fail("Failed to get Delegates", e);
        }
    }
}
