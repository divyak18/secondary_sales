package com.squer.test.platform.meta.reference;

import com.squer.platform.services.entity.impl.SquerNamedReferenceImpl;

/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public class TestNamedReference extends SquerNamedReferenceImpl{

    public TestNamedReference(String id, String name){
        super(id, name);
    }
}
