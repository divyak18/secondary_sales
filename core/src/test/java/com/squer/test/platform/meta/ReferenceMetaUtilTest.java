package com.squer.test.platform.meta;

import com.squer.platform.core.AppConfig;
import com.squer.platform.core.reference.AppConfigReference;
import com.squer.platform.meta.ReferenceMeta;
import com.squer.platform.meta.ReferenceMetaUtil;
import com.squer.platform.persistence.EntityUtil;
import com.squer.platform.security.UserEntity;
import com.squer.test.platform.meta.reference.TestNamedReference;
import com.squer.test.platform.meta.reference.TestReference;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by ashutoshpavaskar on 28/11/16.
 */
public class ReferenceMetaUtilTest {

    @Test(groups = {"platform"})
    public void getReferenceMetaTest(){
        try{
            ReferenceMeta meta = ReferenceMetaUtil.getReferenceMeta("appcg");
            Assert.assertNotNull(meta);
            Assert.assertEquals(meta.getPrefix(),"appcg");
            Assert.assertEquals(meta.getEntityClassName(),AppConfig.class.getName());
            Assert.assertEquals(meta.getTableName(),"FMK_APP_CONFIG");
            Assert.assertEquals(meta.getReferenceClassName(), AppConfigReference.class.getName());
            Assert.assertEquals(meta.getModel(),"BaseModel");
        }catch(Exception e){
            Assert.fail("Get reference by Prefix failed", e);
        }
    }


}
