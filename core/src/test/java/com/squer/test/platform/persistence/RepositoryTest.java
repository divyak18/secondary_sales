package com.squer.test.platform.persistence;

import com.squer.platform.core.AppConfig;
import com.squer.platform.core.reference.AppConfigReference;
import com.squer.platform.persistence.Repository;
import com.squer.platform.persistence.impl.PlatformDelegates;
import com.squer.platform.security.ServiceLocator;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by ashutoshpavaskar on 05/12/16.
 */
public class RepositoryTest {

    private AppConfigReference reference = new AppConfigReference(null);

    @Test(groups = {"platform"})
    public void testCreate(){
        try{
            AppConfig config = new AppConfig();
            config.setAppVersion("1.0");
            config.setAdminPassword("welcome");
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            reference = repository.create(config);
            Assert.assertNotNull(reference);
        }catch (Exception e){
            Assert.fail(e.getMessage(), e);
        }
    }

    @Test(groups = {"platform"}, dependsOnMethods = {"testCreate"})
    public void testRestore(){
        try{
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            AppConfig config = repository.restore(reference);
            Assert.assertNotNull(config);
            Assert.assertEquals(config.getAdminPassword(),"welcome");
            Assert.assertEquals(config.getAppVersion(), "1.0");
        }catch (Exception e){
            Assert.fail(e.getMessage(), e);
        }
    }

    @Test(groups={"platform"}, dependsOnMethods = {"testCreate","testRestore"})
    public void testDelete(){
        try{
            Repository repository = (Repository) ServiceLocator.getInstance().getBean(PlatformDelegates.Repository);
            repository.delete(reference);
        }catch (Exception e){
            Assert.fail(e.getMessage(), e);
        }
    }


}
