package com.squer.test.platform.meta.reference;

import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.impl.AuditableImpl;

/**
 * Created by ashutoshpavaskar on 29/11/16.
 */
public class TestEntity extends AuditableImpl {
    @Attribute(column = "test_field")
    private String testField;

    @Attribute()
    private String testField2;

    public String getTestField() {
        return testField;
    }

    public void setTestField(String testField) {
        this.testField = testField;
    }

    public String getTestField2() {
        return testField2;
    }

    public void setTestField2(String testField2) {
        this.testField2 = testField2;
    }
}
