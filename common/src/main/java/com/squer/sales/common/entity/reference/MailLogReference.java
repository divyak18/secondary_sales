package com.squer.sales.common.entity.reference;

import com.squer.platform.services.entity.impl.SquerReferenceImpl;

public class MailLogReference extends SquerReferenceImpl {

    public MailLogReference() {
    }

    public MailLogReference(String id) {
        super(id);
    }
}
