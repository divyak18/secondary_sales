package com.squer.sales.common.entity;

public enum MailStatus {
    NEW_MAIL("New")
    ,NEW_ATTACHMENT("NEW")
    ,UNPARSABLE("Unparsable")
    ,STOCKIST_NOT_FOUND("Stockist not found")
    ,INCOMPLETE_STATEMENT("Incomplete")
    ;
    private String status;
    private MailStatus(String status) {
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

}
