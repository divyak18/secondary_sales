package com.squer.sales.common.entity.reference;

import com.squer.platform.services.entity.impl.SquerReferenceImpl;

public class MailAttachmentReference extends SquerReferenceImpl {

    public MailAttachmentReference() {
    }

    public MailAttachmentReference(String id) {
        super(id);
    }
}
