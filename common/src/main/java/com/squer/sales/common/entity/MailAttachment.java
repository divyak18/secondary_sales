package com.squer.sales.common.entity;

import com.squer.platform.services.entity.impl.AuditableImpl;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;
import com.squer.sales.common.entity.reference.MailLogReference;

@Entity(prefix = "MATTC", table = "CMT_MAIL_ATTACHMENT", module = "common")
public class MailAttachment extends AuditableImpl {

    @Attribute(dbType = "ID", column = "MAIL_LOG_ID")
    private MailLogReference log;

    @Attribute(dbType = "BIGINT", column = "MESSAGE_ID")
    private Long messageId;

    @Attribute(dbType = "L_STRING", column = "ATTACHMENT_NAME")
    private String attachmentName;

    @Attribute(dbType = "L_STRING", column = "ATTACHMENT_PATH")
    private String path;

    @Attribute(dbType = "M_STRING", column = "STATUS")
    private String status;

    @Attribute(dbType="S_STRING", column = "STOCKIST_CODE")
    private String stockistCode;

    @Attribute(dbType = "NUMERIC(10,2)", column = "AMOUNT")
    private Double amount;


    public String getAttachmentName() {
        return attachmentName;
    }

    public void setAttachmentName(String attachmentName) {
        this.attachmentName = attachmentName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStockistCode() {
        return stockistCode;
    }

    public void setStockistCode(String stockistCode) {
        this.stockistCode = stockistCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public MailLogReference getLog() {
        return log;
    }

    public void setLog(MailLogReference log) {
        this.log = log;
    }
}
