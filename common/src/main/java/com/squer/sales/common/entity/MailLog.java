package com.squer.sales.common.entity;

import com.squer.platform.services.entity.impl.AuditableImpl;
import com.squer.platform.services.entity.meta.Attribute;
import com.squer.platform.services.entity.meta.Entity;

import java.util.Date;

@Entity(prefix = "MLOGS", table = "CMT_MAIL_LOG", module = "common")
public class MailLog extends AuditableImpl {

    @Attribute(column = "MESSAGE_ID", dbType = "BIGINT")
    private Long messageId;

    @Attribute(column = "DATE_RECEIVED", dbType = "TIMESTAMP")
    private Date dateReceived;

    @Attribute(column = "STATUS", dbType = "M_STRING")
    private String status;

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Date getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(Date dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
